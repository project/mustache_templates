<?php

namespace Drupal\Tests\mustache\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\user\Entity\User;

/**
 * Kernel tests for inline templates.
 *
 * @group mustache
 */
class InlineTemplatesTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'user',
    'field',
    'mustache',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);
    User::create(['uid' => 1, 'name' => 'admin'])->save();
  }

  /**
   * Tests rendering of inline templates.
   */
  public function testRenderInline() {
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    $render_array = MustacheRenderTemplate::build('my_inline_template', 'Value: {{ baz.qux }}')
      ->usingData(['foo' => ['bar' => ['baz' => ['qux' => 'Hello!']]]])
      ->selectingSubsetFromData(['foo', 'bar'])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Value: Hello!', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', 'Todo:{{#.}} - {{.}}{{/.}}')
      ->usingData(['One', 'Two', 'Three'])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Todo: - One - Two - Three', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', 'Todo:{{#mytodo}} - {{.}}{{/mytodo}}')
      ->usingData(['mytodo' => ['One', 'Two', 'Three']])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Todo: - One - Two - Three', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', 'Todo:{{#mytodo.list}} - {{.}}{{/mytodo.list}}')
      ->usingData(['mytodo' => ['list' => ['One', 'Two', 'Three']]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Todo: - One - Two - Three', $rendered);

    $admin_user = User::load(1);
    $render_array = MustacheRenderTemplate::build('my_inline_template', 'User name: {{ user.account-name }}')
      ->withTokens(['user' => $admin_user])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('User name:', $rendered, 'User without permission is not allowed to view Token data.');
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#user}}User name: {{ user.account-name }}{{/user}}')
      ->withTokens(['user' => $admin_user])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('', $rendered, 'User without permission is not allowed to view Token data.');
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#user.uid}}User name: {{ user.account-name }}{{/user.uid}}')
      ->withTokens(['user' => $admin_user])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('', $rendered, 'User without permission is not allowed to view Token data.');
    $account_switcher->switchTo($admin_user);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#user}}User name: {{ user.account-name }}{{/user}}')
      ->withTokens(['user' => $admin_user])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('User name: admin', $rendered, 'User with permission is allowed to view Token data.');
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#user.account-name}}User name: {{ user.account-name }}{{/user.account-name}}')
    ->withTokens(['user' => $admin_user])
    ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('User name: admin', $rendered, 'User with permission is allowed to view Token data.');
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#user.uid}}User name: {{ user.account-name }}{{/user.uid}}')
    ->withTokens(['user' => $admin_user])
    ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('User name: admin', $rendered, 'User with permission is allowed to view Token data.');
    $account_switcher->switchBack();
  }

  /**
   * Tests magic conditions using inline templates.
   */
  public function testMagicConditions() {
    $admin_user = User::load(1);
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo($admin_user);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This SHOULD show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}')
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD show up!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{^if.foo.bar.defined}}This SHOULD show up!{{/if.foo.bar.defined}}{{#if.foo.bar.defined}}This should not show up!{{/if.foo.bar.defined}}')
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD show up!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This SHOULD show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}')
      ->usingData(['foo' => ['bar' => []]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD show up!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This SHOULD show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}')
      ->usingData(['foo' => ['bar' => '']])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD show up!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This SHOULD show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}')
      ->usingData(['foo' => ['bar' => 0]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD show up!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.numeric}}Is numeric!{{/if.foo.bar.numeric}}{{^if.foo.bar.numeric}}Is not numeric!{{/if.foo.bar.numeric}}')
      ->usingData(['foo' => ['bar' => 0]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Is numeric!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This SHOULD should show up :){{/if.foo.bar.empty}}')
      ->usingData(['foo' => ['bar' => 1]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD should show up :)', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.numeric}}Is numeric!{{/if.foo.bar.numeric}}{{^if.foo.bar.numeric}}Is not numeric!{{/if.foo.bar.numeric}}')
      ->usingData(['foo' => ['bar' => 0]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Is numeric!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.bar.empty}}This should not show up!{{/if.foo.bar.empty}}{{^if.foo.bar.empty}}This SHOULD should show up :){{/if.foo.bar.empty}}')
      ->usingData(['foo' => ['bar' => ['baz' => 'qux']]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This SHOULD should show up :)', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.greaterthan.150}}This should not show up!{{/if.foo.greaterthan.150}}{{#if.foo.greaterthan.100}}GREATER!{{/if.foo.greaterthan.100}}')
      ->usingData(['foo' => 123])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.smallerthan.0}}This should not show up!{{/if.foo.smallerthan.0}}{{#if.foo.smallerthan.124}}SMALLER!{{/if.foo.smallerthan.124}}')
      ->usingData(['foo' => 123])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('SMALLER!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.atleast.150}}This should not show up!{{/if.foo.atleast.150}}{{#if.foo.atleast.100}}AT LEAST!{{/if.foo.atleast.100}}')
      ->usingData(['foo' => 123])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('AT LEAST!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.foo.atmost.100}}This should not show up!{{/if.foo.atmost.100}}{{#if.foo.atmost.123}}AT MOST!{{/if.foo.atmost.123}}')
      ->usingData(['foo' => "123"])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('AT MOST!', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.mytodo.list.atleast.100}}This should not show up!{{/if.mytodo.list.atleast.100}}{{#if.mytodo.list.atleast.2}}AT LEAST!{{/if.mytodo.list.atleast.2}}')
      ->usingData(['mytodo' => ['list' => ['First item', 'Second item', 'Third item']]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('AT LEAST!', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.mytodo.list.atmost.2}}This should not show up!{{/if.mytodo.list.atmost.2}}{{#if.mytodo.list.atmost.3}}AT MOST!{{/if.mytodo.list.atmost.3}}')
      ->usingData(['mytodo' => ['list' => ['First item', 'Second item', 'Third item']]])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('AT MOST!', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.mytodo.1.equals.Two}}EQUALS!{{/if.mytodo.1.equals.Two}}')
      ->usingData(['mytodo' => ['One', 'Two', 'Three', 'Four']])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('EQUALS!', $rendered);
    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.mytodo.1.equals.Two}}EQUALS!{{/if.mytodo.1.equals.Two}}')
      ->usingData(['mytodo' => ['Zero', 'One', 'Two', 'Three', 'Four']])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('', $rendered);

    $render_array = MustacheRenderTemplate::build('my_inline_template', '{{#if.mytodo.0.equals.One}}EQUALS!{{/if.mytodo.0.equals.One}}')
      ->usingData(['mytodo' => ['One', 'Two', 'Three', 'Four']])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('EQUALS!', $rendered);

    $account_switcher->switchBack();
  }

  /**
   * Tests magic filters using inline templates.
   */
  public function testMagicFilters() {
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');

    $render_array = MustacheRenderTemplate::build('filter_template', ' {{#filter.trim}}          Hello!       {{/filter.trim}} ')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals(' Hello! ', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.striptags}}<h1><strong>Hello</strong> <span>there!</span></h1>{{/filter.striptags}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('Hello there!', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', "{{#filter.nl2br}}Hello\nthere!{{/filter.nl2br}}")
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals("Hello<br />\nthere!", $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.spaceless}}<p> <b>Hello there! </b></p>{{/filter.spaceless}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals("<p><b>Hello there! </b></p>", $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.upper}}Hello!{{/filter.upper}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('HELLO!', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.lower}}Hello!{{/filter.lower}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('hello!', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.capitalize}}hello!{{/filter.capitalize}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('Hello!', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.truncate}}aaaaa bbbbbbb ccccccccc dddddddd eeee fffffffff ggggg{{/filter.truncate}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('aaaaa bbbbbbb ccccccccc dddddddd eeee fffffffff ggggg', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.truncate.30}}aaaaa bbbbbbb ccccccccc dddddddd eeee fffffffff ggggg{{/filter.truncate.30}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('aaaaa bbbbbbb ccccccccc…', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.path}}Hello There/GoodBye{{/filter.path}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('hello%20there/goodbye', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.plus.10}}1{{/filter.plus.10}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertEquals('11', $render_array['#content']);

    $render_array = MustacheRenderTemplate::build('filter_template', '{{#filter.minus.1}}1{{/filter.minus.1}}')
      ->toRenderArray();
    $renderer->renderPlain($render_array);
    $this->assertSame('0', (string) $render_array['#content']);
  }

}
