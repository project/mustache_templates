<?php

namespace Drupal\Tests\mustache\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\user\Entity\User;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Plugin\LanguageNegotiation\LanguageNegotiationUser;

/**
 * Kernel tests for magic translations.
 *
 * @group mustache
 */
class MagicTranslationTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'user',
    'field',
    'filter',
    'text',
    'node',
    'language',
    'content_translation',
    'locale',
    'locale_test',
    'mustache',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installSchema('node', ['node_access']);
    $this->installSchema('locale', [
      'locales_source',
      'locales_target',
      'locales_location',
      'locale_file',
    ]);
    $this->installConfig(static::$modules);
    User::create(['uid' => 1, 'name' => 'admin'])->save();

    ConfigurableLanguage::create(['id' => 'de'])->save();
    // Set up language negotiation.
    $config = $this->config('language.types');
    $config->set('configurable', [
      LanguageInterface::TYPE_INTERFACE,
      LanguageInterface::TYPE_CONTENT,
    ]);
    $config->set('negotiation', [
      LanguageInterface::TYPE_INTERFACE => [
        'enabled' => [LanguageNegotiationUser::METHOD_ID => 0],
      ],
      LanguageInterface::TYPE_CONTENT => [
        'enabled' => [LanguageNegotiationUrl::METHOD_ID => 0],
      ],
    ]);
    $config->save();
    $config = $this->config('language.negotiation');

    /** @var \Drupal\locale\StringStorageInterface $string_storage */
    $string_storage = \Drupal::service('locale.storage');
    $string_storage->createString(['source' => 'This text should be translated...', 'context' => ''])->save();
    $string = $string_storage->findString(['source' => 'This text should be translated...', 'context' => '']);
    $lid = $string->getId();
    $string_storage->createTranslation([
      'lid' => $lid,
      'language' => 'de',
      'translation' => 'Dieser Text sollte übersetzt werden...',
    ])->save();
    _locale_refresh_translations(['de'], [$lid]);

    // Create the Article content type with revisioning and translation enabled.
    /** @var \Drupal\node\NodeTypeInterface $node_type */
    $node_type = NodeType::create([
      'type' => 'article',
      'name' => 'Article',
      'new_revision' => TRUE,
    ]);
    $node_type->save();
    ContentLanguageSettings::create([
      'id' => 'node.article',
      'target_entity_type_id' => 'node',
      'target_bundle' => 'article',
      'default_langcode' => LanguageInterface::LANGCODE_DEFAULT,
      'language_alterable' => TRUE,
    ])->save();

    /** @var \Drupal\node\NodeInterface $node */
    $node = Node::create([
      'type' => 'article',
      'title' => 'English title',
      'langcode' => 'en',
      'uid' => 1,
      'status' => 1,
    ]);
    $node->save();
    $node->addTranslation('de', [
      'type' => 'article',
      'title' => 'Deutscher Titel',
      'langcode' => 'de',
      'uid' => 1,
      'status' => 1,
    ])->save();

  }

  /**
   * Tests magic translations using inline templates.
   */
  public function testInline() {
    $admin_user = User::load(1);
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo($admin_user);

    $render_array = MustacheRenderTemplate::build('another_template', '{{#t.en}}This text should be translated...{{/t.en}}')
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('This text should be translated...', $rendered);
    $render_array = MustacheRenderTemplate::build('another_template', '{{#t.de}}This text should be translated...{{/t.de}}')
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Dieser Text sollte übersetzt werden...', $rendered);

    $render_array = MustacheRenderTemplate::build('another_one', '{{#t.de}}{{text}}{{/t.de}}')
      ->usingData(['text' => 'This text should be translated...'])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Dieser Text sollte übersetzt werden...', $rendered);

    $node = current(\Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['title' => 'English title']));
    $render_array = MustacheRenderTemplate::build('another_template', '{{#t.i}}{{node.title}}{{/t.i}}')
      ->withTokens(['node' => $node])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('English title', $rendered);
    $render_array = MustacheRenderTemplate::build('another_template', '{{#t.de}}{{node.title}}{{/t.de}}')
    ->withTokens(['node' => $node])
    ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('Deutscher Titel', $rendered);

    $account_switcher->switchBack();
  }

}
