<?php

namespace Drupal\Tests\mustache\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\user\Entity\User;

/**
 * Kernel tests for templates using tokens.
 *
 * @group mustache
 */
class TokenTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'user',
    'field',
    'mustache',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);
    User::create(['uid' => 1, 'name' => 'admin'])->save();
    User::create(['uid' => 2, 'name' => 'user2'])->save();
    User::create(['uid' => 3, 'name' => 'user3'])->save();
  }

  /**
   * Tests rendering of tokens.
   */
  public function testTokens() {
    $admin_user = User::load(1);
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    $account_switcher->switchTo($admin_user);
    $render_array = MustacheRenderTemplate::build('user', 'UID: {{user.uid}}')
      ->withTokens()
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('UID: 1', $rendered);

    $render_array = MustacheRenderTemplate::build('user', 'UID: {{user.uid}}')
      ->withTokens(['user' => User::load(2)])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('UID: 2', $rendered);

    $render_array = MustacheRenderTemplate::build('user', '{{#if.user.uid.lessthan.9}}OK{{/if.user.uid.lessthan.9}}')
      ->withTokens(['user' => User::load(3)])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('OK', $rendered);

    $render_array = MustacheRenderTemplate::build('user', '{{#if.user.uid.lessthan.2}}NOT OK{{/if.user.uid.lessthan.2}}{{^if.user.uid.lessthan.2}}OK{{/if.user.uid.lessthan.2}}')
      ->withTokens(['user' => User::load(3)])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('OK', $rendered);

    $render_array = MustacheRenderTemplate::build('user', '{{#if.user.uid.greaterthan.2}}OK{{/if.user.uid.greaterthan.2}}{{^if.user.uid.greaterthan.2}}NOT OK{{/if.user.uid.greaterthan.2}}')
      ->withTokens(['user' => User::load(3)])
      ->toRenderArray();
    $rendered = trim((string) $renderer->renderPlain($render_array));
    $this->assertEquals('OK', $rendered);

    $render_array = MustacheRenderTemplate::build('user', '{{#if.user.uid.greaterthan.5}}NOT OK{{/if.user.uid.greaterthan.5}}{{^if.user.uid.greaterthan.5}}OK{{/if.user.uid.greaterthan.5}}')
    ->withTokens(['user' => User::load(3)])
    ->toRenderArray();
  $rendered = trim((string) $renderer->renderPlain($render_array));
  $this->assertEquals('OK', $rendered);

    $account_switcher->switchBack();
  }

}
