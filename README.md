# Mustache Logic-less Templates

**Integrates Mustache.php plus Mustache.js into Drupal.**

Using Mustache.php by
 * Copyright (c) 2010-2015 Justin Hileman

Using Mustache.js by
* Copyright (c) 2009 Chris Wanstrath (Ruby)
* Copyright (c) 2010-2014 Jan Lehnardt (JavaScript)
* Copyright (c) 2010-2015 The mustache.js community

Using morphdom by
* Copyright (c) Patrick Steele-Idem

## 0. Contents

* 1. Introduction
* 2. Installation
* 3. Usage
* 4. API
* 5. Internal module development notices

## 1. Introduction

This module enhances Drupal with `{{Mustache}}` - a simple yet powerful
templating language.

Features:
* Write and load Mustache templates within your theme or module - optimized
  caching included.
* Ever had the need of replacing some parts of your server-side rendered DOM in
  the browser by fetching some data from a Json resource? This module got you
  covered. It provides a simple, efficient and fault tolerant mechanic for
  fetching and printing data of external RESTful APIs. Covers both the
  consumption on the server-side with Mustache.php as well as on the client-side
  with Mustache.js. No need to reinvent the wheel for simple API consuming.
* Ever came across the limitations of the Token system, which is good enough for
  simple string replacements, but does not support conditions and loops?
  By enabling the "Mustache Logic-Less Tokens" sub-module, you can have
  conditionals and loops by using Mustache template syntax, for example on
  Metatag input fields.
* Enable users to write dynamic content without the need of a full-fledged
  template language like Twig or PHP. If you consider providing a "low-code"
  platform (Digital Experience Platform, DXP), then you could consider using
  this module to accomplish that.
* If you deliver AMP pages and ever need to display dynamic stuff in there, you
  can do so with Mustache templates.

Optional sub-modules included:
* "Mustache Logic-less Tokens" (`mustache_token`): Automatically enhances the
  Token system everywhere token replacement is possible (see section 3.2).
* "Mustache Logic-less Views" (`mustache_views`): Use Mustache variables
  including Tokens within Views configurations. For example, you can use a
  variable in the input for a fixed default argument within contextual filters.
  More about this sub-module is described in section 3.8.
* "Mustache Logic-less Templates: Even more magic" (`mustache_magic`):
  Adds more magic variable plugins to Mustache Templates, for example {{#sync}}.
  More about this module see section 3.6.
* "Tests: Mustache Logic-less Templates" (`mustache_test`): Includes various
  test pages to see whether Mustache templates work on your site. Only install
  it on development or local testing environments, do not install it on
  production environments. Once installed, you can access various test pages,
  for example `/mustache-test/interactive`.

## 2. Installation

When using Composer, add the required packages to your project with it:
```bash
composer require "mustache/mustache:^2.13"
composer require "drupal/mustache_templates:^2.0@rc"
```
Then, install the module via `/admin/modules` or drush:
```bash
drush en mustache
```

When you finished the above mentioned steps, then you should now have all
required libraries installed, and you are ready to start using Mustache
templates on your Drupal site.

If you want to develop with Mustache templates, have a look at section 4.

Further information regards Composer-based workflows:
https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies

Here is a summary of all required libraries that should now be installed:
- Mustache.php by bobthecow v2.13.0 or newer as vendor library
  https://github.com/bobthecow/mustache.php
- Mustache.js by janl v4.2.0 or newer as Drupal Javascript library
  https://github.com/janl/mustache.js
  A minified version of this library is already included in this module, so
  it is not required to download it manually. It might be that this module
  does not include the most recent version of the Javascript library though.
  You can always download the library on your own and place it into the Drupal
  app root directory <DRUPAL_APP_ROOT>/libraries/js/mustache/mustache.min.js.
  If you want to download a minified version, it is available in the npm
  repository https://www.npmjs.com/package/mustache.
- Morphdom by Patrick Steele-Idem v2.6.1 or newer as Drupal Javascript library
  https://github.com/patrick-steele-idem/morphdom
  A minified version of this library is already included in this module, so
  it is not required to download it manually. It might be that this module
  does not include the most recent version of the Javascript library though.
  You can always download the library on your own and place it into the Drupal
  app root directory <DRUPAL_APP_ROOT>/libraries/js/morphdom/morphdom-umd.min.js.
  If you want to download a minified version, it is available in the npm
  repository https://www.npmjs.com/package/morphdom.

## 3. Usage

### 3.1 The Mustache text filter

For trusted users with advanced permissions (e.g. technical admins or trusted
editors), a text filter plugin is available, that allows to write Mustache
template syntax for creating dynamic content. This text processor includes token
support out of the box, thus not requiring an extra module to also include
tokens.

The Mustache filter enables you to use tokens in a more dynamic way, e.g.
enclosing them with a condition:

```html
<h1>Welcome to {{site.name}}</h1>
{{#site.slogan}}
  <h2>{{site.slogan}}</h2>
{{/site.slogan}}
```

Beware that this templating engine enables you to bypass escaping by using
the triple bracket syntax `{{{}}}`. Because of that you should only give trusted
users the permission to use that filter.

It is very likely that this filter plugin does not work well in combination
with other filters, that involve any further dynamic processing like another
token filter plugin. For example, the token filter by the contrib Token Filter
module should not be used together with the Mustache filter on the same text format.

### 3.2 The Mustache Logic-less Tokens sub-module

If you want to enable site users to use Mustache token syntax within every input
that allows token replacements, then additionally install the "Mustache
Logic-less Tokens" sub-module, which is already included in this module.

This sub-module automatically enhances the Token system everywhere token
replacement is possible, with the abilities (and implications) described in the
previous section.

The drush command to install it would be:
```bash
drush en mustache_token
```

When using this sub-module, the contrib Token module is recommended to be
installed: https://drupal.org/project/token

When the contrib Token this module is installed, you should see a help page
available at `/admin/help/token`. On that page, when looking at the displayed
token tree, you can see a new token type "iterate". This special type comes
from the `mustache_token` module and it enables you to perform iterations.

Here is an example using an `iterate:loop` token:
```html
<h1>To Do</h1>
<ul>
  {{#iterate.loop.node.field_mytodolist}}
    <li>{{#first}}First step: {{/first}}{{^first}}Next step no. {{position}}: {{/first}}{{.}}{{#last}}</li><li>... and take care of yourself.{{/last}}</li>
  {{/iterate.loop.node.field_mytodolist}}
</ul>
```

In the example above, you may have noticed some "magical" keywords that may be
used within the scope of an iteration:
* `{{first}}` is a boolean indicating whether the current item is the first one.
* `{{last}}` is a boolean indicating whether the current item is the last one.
* `{{position}}` is a whole number (starting at 1) of the item's position.
* `{{.}}` is the printable text value of the scoped item.

Some other "magical" variables might (or might not) be available:
* `{{parent}}` might allow accessing the parent item which holds the item list.
  Also, accessing specific items might be possible too, e.g. `{{parent.0}}`.
* `{{value}}` might give an alternative text output besides `{{.}}`.
* `{{string}}` might give an alternative text output besides `{{value}}`.

### 3.3 How to find and use available data

Sometimes, it may be not clear which data is available when writing dynamic
content. When using the Mustache text filter plugin (see 3.1) or any token
input with the `mustache_token` sub-module (see 3.2), then you can look
up for available tokens. The contrib Token module provides a UI for browsing
for available tokens via `/admin/help/token`.

Another option is to use a "magical" `{{show.keys}}` variable directly in your
Mustache template file or input field. It gives you the ability to see available
data keys - they will be printed out as a message once it is set like the
following:
```html
{{show.keys}}
```

For being able to see the printed introspection messages, a user needs the
"View Mustache debug messages" permission, that may be set up via
`/admin/people/permissions`. Don't forget to remove that {{show.keys}} variable
from your template once you won't need it anymore.

It might be that the introspection message will print a lot of information. That
is planned to be improved in future releases. We also plan to provide a UI
widget that enables to browse through and preview available data. Feel free to
join the ongoing development:
- https://www.drupal.org/project/mustache_templates/issues/3245567
Also feel free to contribute to this project directly via issue queue:
- https://www.drupal.org/project/issues/mustache_templates
Any sort of feedback and improvements is always welcome!

### 3.4 Conditions

Mustache templates already allow for simple condition checks using sections,
for example

```html
{{^site.slogan}}This site has no slogan.{{/site.slogan}}
```

As such simple boolean checks may not be enough, this module provides a so
called "magical" variable for you - the `{{#if}}` variable. It allows to perform
a little bit more advanced checks. It has the following pattern:

```html
{{#if.<data>.<condition>}}
```

Here are some examples:

```html
{{#if.node.title.empty}}Title is empty!{{/if.node.title.empty}}
{{^if.node.id.numeric}}ID is not numeric...{{/if.node.id.numeric}}
{{#if.user.uid.equals.1}}You are the admin.{{/if.user.uid.equals.1}}
{{#if.user.uid.greaterthan.1}}You are authenticated.{{/if.user.uid.greaterthan.1}}
{{#if.user.uid.lessthan.1}}You are anonymous.{{/if.user.uid.lessthan.1}}
{{#if.node.field_mytodolist.atleast.1}}You have work to do.{{/if.node.field_mytodolist.atleast.1}}
{{#if.node.field_mylazylist.atmost.0}}No time to be lazy.{{/if.node.field_mylazylist.atmost.1}}
```

Conditions are possible on scalar values like numbers and strings. Lists are
also supported, and the number of items will be used for comparison.

### 3.5 Previous data

It is sometimes possible to access previous data with `{{previous.<b>*</b>}}`.
Please note that the availability of previous data is not reliable, and using
values from it should not be done within the scope of business critical data.

When available, conditions are possible regards previous data. Examples:

```html
{{#if.previous.node.title.empty}}
  That one was new, it seems.
{{/if.previous.node.title.empty}}
{{#if.previous.user.uid.equals.1}}
  You were supposed to be the admin
{{/if.previous.user.uid.equals.1}}
{{#if.previous.user.uid.lessthan.1}}{{#if.user.uid.greaterthan.0}}
  You just logged in, welcome on board.
{{/if.user.uid.greaterthan.0}}{{/if.previous.user.uid.lessthan.1}}
{{#if.node.field_mytodolist.greaterthan.previous}}
  Great, now there's even more to do...
{{/if.node.field_mytodolist.greaterthan.previous}}
```

### 3.6 Magic synchronization

The already included and optional sub-module "Mustache Logic-less Templates:
Even more magic" (`mustache_magic`) adds a further "magical" `{{#sync}}`
variable. This variable enables automated DOM synchronization within any input
that supports Mustache template syntax.

Examples:

```html
One time immediate refresh:
{{#sync.now}}{{node.title}}{{/sync.now}}

Unlimited refresh every 10 seconds:
{{#sync.now.10.always}}...{{/sync.now.10.always}}

Up to 10 refreshes every second (wrapped by an inline <span>):
{{#sync.now.1.10.span}}...{{/sync.now.1.10.span}}

Up to 10 refreshes every second, delayed by 5 seconds:
{{#sync.5.1.10}}...{{/sync.5.1.10}}

Trigger refresh when clicking a DOM element having the CSS class ".button":
{{#sync.trigger.button.click}}...{{/sync.trigger.button.click}}

Use a custom URL to fetch and display its JSON-formatted data:
{{#sync.now.1.always.url./my/json-url}}
  {{something_from_the_url}}
{{/sync.now.1.always.url./my/json-url}}

Use your own web server as proxy for fetching data:
{{#sync.now.url.https://www_dot_my-external-site_dot_com/my/json-url.proxy}}
  {{something_from_the_url}}
{{/sync.now.url.https://www_dot_my-external-site_dot_com/my/json-url.proxy}}

Bind a form to a URL (i.e. form input will be passed as query parameters), and
print out submitted form values:
<form class="myform"><input type="text" class="mytext" name="search"/></form>
{{#sync.now.form.myform.trigger.mytext.input.always}}
  Your search for <em>{{form.search}}</em>:
  {{^results}}No results found.{{/results}}
  {{#results}}{{results}}{{/results}}
{{/sync.now.form.myform.trigger.mytext.input.always}}
```

It should be noted, that the usage of the magical `{{#sync}}` variable injects
Javascript logic and wraps the synchronization target with an additional DOM
element. Thus it should not be used within areas where HTML is not supported
(for example plain text fields).

As can be seen in the examples above, `{{#sync}}` allows a concatenation of
various options, i.e. `{{#sync.<options>[...]}}`. It needs at least one option,
for example `{{#sync.now}}`. Available `<options>` are:

* `now` means immediate execution of the sync instruction. It can only be set
  as first option, i.e. `{{#sync.now.[...]}}`
* `always` means unlimited repetitions of the prepended instruction. Examples:
  `{{#sync.now.1.always}}`, `{{#sync.now.trigger.mybutton.click.always}}`
* `once` means one-time execution of the prepended instruction. Examples:
  `{{#sync.now.1.once}}`, `{{#sync.now.trigger.mybutton.click.once}}`
* `span` wraps the synchronizing DOM target element with a `<span>` instead
  of a `<div>`.
* `form.<selector>` enables automated form binding. It requires the
  definition of the CSS selector that identifies the DOM element of the form.
  Example: `{{#sync.now.form.myform}}` enables automated form binding to the
  form having the CSS class "myform", e.g. `<form class="myform">`. If you
  want to use an element ID instead of a class, prepend with a "#", for example
  `{{#sync.now.form.#myform}}` will bind to `<form id="myform">`.
* `trigger.<selector>.<event>.<limit>` enables triggering of synchronization
  when the specified event on the given CSS selector is being fired. Example:
  `{{#sync.now.trigger.refreshbutton.click.always}}` will trigger on an element
  having the CSS class "refreshbutton", i.e. it will be triggered when clicked
  on e.g. `<div class="refreshbutton"></div>`. IF you want to use an element ID,
  prepend with a "#" (as the same goes for `form.<selector>`).
* `into.<selector>` uses the DOM element identified by a given CSS selector as
  synchronization target. By default, the synchronizing template contents would
  be wrapped by a `<div>` (or by a `<span>` when the above mentioned `span`
  option is being used). When using this option, that behavior changes -
  instead of creating a wrapper element, the specified element will be
  identified and used for inserting rendered results. Example:
  ```html
  {{#sync.now.10.always.into.head>title}}
    {{site.name}} (synced!)
  {{/sync.now.10.always.into.head>title}}
  ```
* `url.<url>` allows to specify a URL where to get the JSON data from. the URL
  may be relative or absolute. Example using a relative URL:
  `{{#sync.now.url.my/own/endpoint}}`. Example using an absolute URL:
  `{{#sync.now.url.https://some-xternalsite_dot_com/the-other-endpoint}}`
  Please note when using an external URL, the protocol (either http or https)
  must be specified at the beginning. Also since a dot `.` would conflict with
  the variable resolution, `_dot_` needs to be used instead of a real dot `.`
  within the URL.
* `proxy` moves the main work of synchronization to the server. Usually, the
  client does the most job to synchronize DOM content by rendering a certain
  template with some data. That usually includes the fetching of data from a
  specified URL. By moving that work to the proxy, the server fetches the data
  from the URL endpoint, performs the rendering and returns the render result to
  the client. This option is mostly useful if you want to:
  * Use Tokens within the contents of your enclosed `{{#sync}}`.
  * Reduce request overload regards the URL endpoint. This is a valid approach
    since the server-side rendering caches received HTTP responses.
* `increment.<key>.<offset>.<limit>.<step-size>` enables automated incrementing
  on the specified key (if not specified, "page" will be used), optionally with
  an offset, limit and step size as integer decimal. Example:
  `{{#sync.now.url.my/own/endpoint.increment}}` would automatically make a
  request on `my/own/endpoint?page=0`, the next fetch call would be
  `my/own/endpoint?page=1`, next `my/own/endpoint?page=2` and so on.
* `summable` enables aggregation, i.e. it will not be embedded as inline
  Javascript - instead it will be provided as JS asset library, so that it can
  be aggregated. This may optimize loading overhead on the client side, in case
  you have the same template on multiple pages (especially when having it on
  *every* page). If the template is kind of unique and not being reused, this
  option should not be used. Example:
  `{{#sync.now.summable}}I will be aggregated, yay!{{/sync.now.summable}}`
* `template.<name>` registers the wrapped contents. If you use this option,
  that template will be automatically aggregated, i.e. the `summable` is
  already included and is not required to be extra added. Example:
  ```html
  {{#sync.now.template.mytemplate}}
    I will be aggregated, and I can be reused as partial "mytemplate", yay!<br/>
    See the Mustache manual (https://mustache.github.io/mustache.5.html) regards
    how to use partials.
  {{/sync.now.template.mytemplate}}
  ```
  Tip: If you want to define a reusable template without using `{{#sync}}` you
  can do so by specifying `{{#template.<name>}}`. Example:
  ```html
  {{#template.greeting}}
    Hello {{name}}!
  {{/template.greeting}}
  {{#if.user.defined}}
    {{#user}}{{>greeting}}{{/user}}
  {{/if.user.defined}}
  ```

### 3.7 Translations

You can use Drupal's translation system within your templates by using the
`{{#t.<langcode>}}` section variable. Wrap your translation target by that
section variable. Any values (including entities) will be translated into the
specified language, including the wrapped template content itself.

You can specify an ISO 639-1 language code as target translation or use either
one of `i`, `interface` and `current` to use the current interface language,
for example `{{#t.current}}`.

Examples:

```html
{{#t.i}}
  I am an english text and will be substituted by an according
  translation of the current interface language.
{{/t.i}}

{{#t.de}}
I am an english text and will be substituted by an according German translation.
{{/t.de}}
```

### 3.8 Mustache in Views

By installing the `mustache_views` sub-module, you may use Mustache variables
including Tokens within Views configurations. For example, you can use a
variable in the input for a fixed default argument within contextual filters.

**Please note**:
When using Mustache variables and Tokens from data that are provided by a
certain context, for example using the node coming from the URL, then you need
to do either one of the following:
* Disable caching for the View (click on `Caching:` within Views UI display
config and choose `None`) or...
* ...add a contextual filter for it. You can do so by choosing `Global: Null`
within contextual filters, and then select "Provide a default value" ->
"Content ID from URL". For the reason why this is necessary, have a look at
https://www.drupal.org/project/mustache_templates/issues/3253881.

### 3.9 Filters

You can use the `{{#filter.<method>}}` section variable to apply a text
processing filter using a specified `<method>` on template contents.

Examples:

```html
{{#filter.trim}}  My whitespace will be trimmed.   {{/filter.trim}}
{{#filter.striptags}}<p>My HTML tags will be removed.</p>{{/filter.striptags}}
{{#filter.nl2br}}The new line \n will be converted to a break tag.{{/filter.nl2br}}
{{#filter.spaceless}}Removes whitespace between HTML tags.{{/filter.spaceless}}
{{#filter.upper}}Makes the whole text uppercase.{{/filter.upper}}
{{#filter.lower}}Makes the whole text lowercase.{{/filter.lower}}
{{#filter.capitalize}}The first letter of the text will be uppercase.{{/filter.capitalize}}
{{#filter.truncate.100}}This text will be truncated when it exceeds 100 characters, and appended with a ... if it was truncated.{{/filter.truncate.100}}
```

## 4. API

### 4.1 How Mustache templating works in Drupal

The usage of Mustache templates is mainly defined via render arrays.
Therefore, the module provides a special render element called 'mustache'.

Mustache templates can be put besides Twig templates
into the templates directory of your theme. A file which is a Mustache template
must end with .mustache.tpl.

Example: A template for rendering social share buttons could be named
as "social-share-buttons.mustache.tpl" and put into yourtheme/templates/.
To render the Mustache template, a render array can be created as follows:

```php
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
];
```

Note that `#template` does not contain the .mustache.tpl file ending.
See also section "Render array examples" below to find out
how to create render arrays for Mustache templates.

In case a module wants to define a Mustache template, it can do so by
implementing `hook_mustache_templates()`. With this hook, you can also
define default values for building the render array. All Mustache template hooks
are documented in the mustache.api.php file of this module.

You don't enjoy writing render arrays? Try out and have a look at the section
"Using the helper class for rendering Mustache templates" below.

### 4.2 Render array examples

You can either build render arrays directly, or build them with the helper
class `MustacheRenderTemplate`.

Using the helper class is preferable, because it's covered by an automated
unit test, and since it's providing a more stable and convenient API,
it will support you on automatically covering upcoming changes
regards building the render array.

```php
// Example 1:
// Renders the template (only) via Mustache.php.
// #data can either be an array or a url pointing to the Json data source.
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
];
// Equivalent using the helper class:
$render = $render = MustacheRenderTemplate::build('social_share_buttons')
->usingData($data)
->toRenderArray();

// Example 2:
// With #select you can pass a certain subset of nested data to the template.
$nested = ['foo' => ['bar' => ['baz' => 'qux']]];
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $nested,
  '#select' => ['foo', 'bar'], // Would pass ['baz' => 'qux']
];

// Example 3:
// Renders the template via Mustache.php,
// and refreshes the content via Mustache.js one time per second.
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $url,
  '#sync' => [
    'period' => 1000, // Milliseconds.
    'limit' => -1, // Unlimited number of refreshes.
  ],
];

// Example 4:
// Renders a placeholder, which will be replaced by the given
// template via Mustache.js using Json data supplied by $url.
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $url,
  '#placeholder' => ['#markup' => '<span>Loading...</span>'],
  '#sync' => [
    'wrapper_tag' => 'span', // Set wrapper tag to be a span (default is div).
  ],
];

// Example 5:
// Renders the template via Mustache.php using $data, and synchronizes
// the content one time via Mustache.js using Json data supplied by $url.
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
  '#sync' => [
    'data' => $url,
  ],
];

// Example 6:
// Just like example 5, but this time the template would only be synchronized,
// when a click event has been triggered on any element with a .button CSS class.
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
  '#sync' => [
    'data' => $url,
    'trigger' => [['.button', 'click', 3]], // Would run up to 3 times. -1 would be unlimited.
  ],
];
// Equivalent using the helper class:
$render = MustacheRenderTemplate::build('social_share_buttons')
->usingData($data)
->withClientSynchronization()
  ->usingDataFromUrl($url)
  ->startsWhenElementWasTriggered('.button')
    ->atEvent('click')
    ->upToNTimes(3)
->toRenderArray();
```

### 4.3 Insertion of synchronized content

By default, the synchronization replaces previously generated content.
You can change this behavior by setting the `adjacent` option, which defines
how rendered content should be inserted:

```php
$render = [
  '#type' => 'mustache',
  '#template' => 'my_item_list',
  '#data' => $data,
  '#sync' => [
    'adjacent' => 'beforeend',
  ],
];
// Equivalent using the helper class:
$render = MustacheRenderTemplate::build('my_item_list')
->usingData($data)
->withClientSynchronization()
  ->usingDataFromUrl($url)
  ->insertsAt('beforeend')
->toRenderArray();
```

The position can be either `beforebegin`, `afterbegin`, `beforeend` or
`afterend`. Have a look at the Javascript function `Element.insertAdjacentHTML`
for more information about inserting HTML with the position parameter.

### 4.4 Script execution

By default - due to possible security implications - scripts inside synchronized
DOM content will not be executed automatically. However, if you want to
let the synchronization process also execute possible included scripts,
you can do so by setting the `eval` parameter to TRUE:

```php
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
  '#sync' => [
    'data' => $url,
    'eval' => TRUE,
  ],
];
// Equivalent using the helper class:
$render = MustacheRenderTemplate::build('social_share_buttons')
->usingData($data)
->withClientSynchronization()
  ->usingDataFromUrl($url)
  ->executesInnerScripts(TRUE)
->toRenderArray();
```

### 4.5 Morphing DOM trees

When using client-side synchronization, by default the DOM content is being
completely replaced. While replacing an existing DOM tree with an entirely new
DOM tree is fast, all of the internal state associated with the existing DOM
nodes will be lost. Instead of always replacing the existing DOM tree with a new
DOM tree, you can use the morphdom framework for DOM tree transformations:

```php
$render = [
  '#type' => 'mustache',
  '#template' => 'social_share_buttons',
  '#data' => $data,
  '#sync' => [
    'data' => $url,
    'morph' => TRUE,
  ],
];
```

Please note that you cannot use the `morph` setting together with the
`adjacent` setting. When `adjacent` is set, the `morph` setting will be ignored.

### 4.6 Auto incrementing

The synchronization framework provides the ability to auto increment on
a certain url query param or data key:

```php
$render = MustacheRenderTemplate::build('my_paged_list')
->withClientSynchronization()
  ->usingDataFromUrl($url)
  ->increments()
->toRenderArray();
```

The `increments()` method enables you to further define the auto increment,
including the step size to increment, on which query param to run incrementing,
how many times it should increment, and whether it should loop or not.

### 4.7 Form binding

Form binding enables you to build simple reactive forms without writing any
Javascript.

You can bind a form to the Mustache render build, so that the form values are
directly provided within the `form` key of the data that is being passed to the
template rendering. For example, if you have an input field of name `search`,
then you can access that value in the template with `{{form.search}}`. When a
url is set for fetching Json-encoded data, then the form values will be passed
to that endpoint as query string parameters. This enables your Json endpoint to
return any requested data for the given form input.

The following example binds a form for client synchronization:

```php
$render = [];
$form = <<<HTML
<div>
    <form id="myform" method="GET" action="">
      <input id="myfield" type="text" name="search" value="" />
    </form>
</div>
HTML;
$build = MustacheRenderTemplate::build('inline_result', '<b>Submitted</b>: {{form.search}}<br /><b>Received</b>: {{echo.search}}');
$build->withPlaceholder(['#markup' => '']); // This is an empty placeholder to skip server-side rendering.
$build->usingDataFromUrl('/my-echo-feed'); // Form values will be automatically passed as query string parameters.
$build->withClientSynchronization()
  ->morphing() // Morphing DOM trees also work with form binding.
  ->usingFormValues('#myform')
  ->startsWhenElementWasTriggered('#myfield')
  ->atEvent('input')
  ->always();

$render[] = $build->toRenderArray();
$render[] = ['#markup' => \Drupal\mustache\Render\Markup::create($form)];
```

The above example skips server-side rendering, but form binding also works with
Mustache.php rendering. To include the form binding also for Mustache.php,
use `\Drupal\mustache\Helpers\MustacheRenderTemplate::usingFormValues`, like the
following example:

```php
$build = MustacheRenderTemplate::build('inline_result', '<b>Submitted</b>: {{form.search}}<br /><b>Received</b>: {{echo.search}}');
$build->usingDataFromUrl('/mustache-test/echo-feed')
  ->usingFormValues($form_state)
  ->withClientSynchronization()
    ->morphing()
    ->startsWhenElementWasTriggered('#myfield')
    ->atEvent('input')
    ->always();
  ;
```

As seen in the example above, you can directly pass a form state object (an
instance of `Drupal\Core\Form\FormStateInterface`). The render build takes care
for automatically binding the form and passes current form values to the
Json endpoint.

It should be noted, when working with Drupal forms, mostly the form will have a
redirect after form submission. When a redirect happens, then the form values
are no longer available. The form redirect may be either disabled, or the
relevant arguments may be attached to the redirect url, then extracted and
(manually) passed to the data array.

For more examples on how to bind forms, have a look at the class
`\Drupal\mustache_test\Form\MustacheTestForm` within the mustache_test module
folder, and have a look at the documented method
`\Drupal\mustache\Helpers\MustacheRenderTemplate::usingFormValues`.

### 4.8 Javascript events

The following events are provided at the specified synchronization targets:

- `mustacheSyncBegin`: Triggered when the synchronization for
  a specific item has just been started.
- `mustacheSyncFinish`: Triggered when the synchronization for
  a specific item has just been finished.

### 4.9 Inline templates

You can write templates on-the-fly by directly defining the template content:
```php
$render = MustacheRenderTemplate::build('my_inline_template', '{{foo}}')
->usingData(['foo' => 'bar'])
->toRenderArray();
```

This might be useful if you want to quickly try out Mustache templates, or you
need to dynamically create template contents. It should be noted that this
method involves a different caching strategy that is not that efficient as
when using registered template files. That is why it is generally recommended
to always use registered template files when possible.

### 4.10 Token support

You can enable Tokens to be included like Mustache variables, and provide custom
data and options for the Token replacement processor:
```php
$render = MustacheRenderTemplate::build('node_inline', '{{node.title}}')
->withTokens(['node' => $node])
->toRenderArray();
```

By default, this processing clears empty token values. You can change that
behavior by explicitly setting the options for the Token replacement processor.
The processing also takes care for loading entities from the global context,
so you do not need to set those values manually. But you can still pass through
your own data.

## 5. Internal module development notices

The minified sync.js was generated with the help of terser
(https://www.npmjs.com/package/terser) using the command
`terser sync.js -o sync.min.js -c -m`

The minified version of the Mustache.js library including its license file was
obtained from the npm repository https://www.npmjs.com/package/mustache.

The minified version of the morphdom library including its license file was
obtained from the npm repository https://www.npmjs.com/package/morphdom.
