{{#js}}
<script id="{{{id}}}">
/** MAGIC {{{name}}} */
(function (sync, Drupal, settings) {
{{{content}}}
}(window.mustacheSync, window.Drupal, window.drupalSettings));
</script>
{{/js}}
