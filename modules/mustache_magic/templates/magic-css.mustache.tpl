{{#css}}
<script id="{{{id}}}">
/** MAGIC {{{name}}} */
(function () {
var styleElement = document.createElement('style');
styleElement.innerHTML = '{{{encoded}}}';
document && document.head && document.head.appendChild(styleElement);
}());
</script>
{{/css}}
