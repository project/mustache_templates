/**
 * @file
 * Magic messages for Mustache templates.
 */

(function (sync, Drupal) {
  var types = ['status', 'warning', 'error'];
  var stackIds = [];
  var stackMessages = [];
  var messages = null;
  var magicMessage = function (message_type, text, render) {
    var i, id, rendered;
    if (!messages) {
      try {
        messages = new Drupal.Message();
      }
      catch (e) {
        if (console) {
          console.error("Failed to initialize message object.");
          return '';
        }
      }
    }
    while (stackMessages.length > 2) {
      messages.remove(stackIds.shift());
      stackMessages.shift();
    }
    rendered = render(text).trim();
    if (rendered.length) {
      i = stackMessages.indexOf(rendered);
      if (i < 0) {
        id = messages.add(rendered, {type: message_type});
        stackIds.push(id);
        stackMessages.push(rendered);
        setTimeout(function (id) {this.remove(id);}.bind(messages, id), 20000);
      }
    }
    return '';
  };
  sync.registry.magic.message = function (buffer) {
    var m = {};
    for (var i = 0; i < types.length; i++) {
      m[types[i]] = function (type) {return magicMessage.bind(this, type);}.bind(this, types[i]);
    }
    return m;
  };
}(mustacheSync, Drupal));
