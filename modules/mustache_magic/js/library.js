/**
 * @file
 * Magic library inclusion for Mustache templates.
 */

(function (sync) {
  var magicLibrary = function (buffer, props) {
    return new Proxy(buffer, Object.assign({
      get: function (buffer, property, receiver) {
        return function () {
          return function (text, render) {
            return render(text);
          };
        };
      },
      has: function(target, prop) {
        return typeof prop === 'string' && prop.indexOf('/') > 0;
      }
    }, props));
  };
  sync.registry.magic.library = function (buffer) {
    return magicLibrary(buffer, {});
  };
}(mustacheSync));
