<?php

namespace Drupal\mustache_magic\Controller;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for server-side synchronization of user-defined templates.
 */
class ProxySyncController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The sync storage.
   *
   * @var \Drupal\mustache_magic\Storage\MustacheSyncStorage
   */
  protected $syncStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|null
   */
  protected $messenger;

  /**
   * The minimum threshold of time in seconds for caching rendered results.
   *
   * @var int
   */
  protected $ttlMin;

  /**
   * A volatility value that is a range around the given TTL minimum value.
   *
   * @var int
   */
  protected $volatility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->renderer = $container->get('renderer');
    $instance->syncStorage = $container->get('mustache.sync_storage');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityRepository = $container->get('entity.repository');
    $instance->currentUser = $container->get('current_user');
    $instance->languageManager = $container->get('language_manager');
    $instance->stringTranslation = $container->get('string_translation');
    $instance->messenger = $container->get('messenger');
    $instance->ttlMin = (int) $container->getParameter('mustache.http_cache_ttl_min');
    $instance->volatility = (int) $container->getParameter('mustache.http_cache_volatility');
    return $instance;
  }

  /**
   * Controller method to get a synchronized template.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The given request.
   *
   * @return \Symfony\Component\HttpFoundation
   *   The response object.
   */
  public function get(Request $request) {
    $cacheability = (new CacheableMetadata())
      ->addCacheContexts(['url.path', 'url.query_args']);
    if (!($hash = $request->query->get('h'))) {
      $response = new CacheableJsonResponse(['reason' => "No query parameters were given."], 400);
      $response->addCacheableDependency($cacheability);
      return $response;
    }

    $max_age = $this->ttlMin - $this->volatility;
    if (!($max_age > 0)) {
      $max_age = $this->volatility;
    }

    if (!($values = $this->syncStorage->get($hash))) {
      $response = new CacheableJsonResponse(['reason' => "No values were found."], 404);
      $cacheability->setCacheMaxAge($max_age);
      $response->setCache([
        's_maxage' => $max_age,
        'max_age' => $max_age,
      ]);
      $response->addCacheableDependency($cacheability);
      return $response;
    }

    $anonymous_account = new AnonymousUserSession();
    $current_user = $this->currentUser->getAccount();
    $langcode = isset($values['langcode']) ? $values['langcode'] : $this->languageManager->getCurrentLanguage()->getId();

    $data_is_protected = FALSE;
    foreach (['data', 'token_data'] as $d_k) {
      if (!empty($values[$d_k])) {
        foreach ($values[$d_k] as $t_key => $t_value) {
          if (is_array($t_value)) {
            $num = count($t_value);
            $entity = NULL;
            if ($num === 3) {
              // Entity type, language ID and entity ID.
              list($entity_type_id, $langcode, $entity_id) = $t_value;
              $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
              if ($entity instanceof TranslatableInterface && $entity->language()->getId() != $langcode) {
                $entity = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : NULL;
              }
            }
            elseif ($num === 2) {
              // Entity type and UUID.
              list($entity_type_id, $uuid) = $t_value;
              $entity = $this->entityRepository->loadEntityByUuid($entity_type_id, $uuid);
            }
            if ($entity instanceof TranslatableInterface && $entity->language()->getId() != $langcode && $entity->hasTranslation($langcode)) {
              $entity = $entity->getTranslation($langcode);
            }
            if ($entity) {
              $cacheability->addCacheableDependency($entity);
              $access_result = $entity->access('view', $anonymous_account, TRUE);
              $cacheability->addCacheableDependency($access_result);
              if ((!$access_result->isAllowed() || $access_result->isForbidden()) && !$current_user->isAnonymous()) {
                $access_result = $entity->access('view', $current_user, TRUE);
                $cacheability->addCacheableDependency($access_result);
                if (!$access_result->isAllowed() || $access_result->isForbidden()) {
                  $response = new CacheableJsonResponse(['reason' => "Not allowed."], 405);
                  $cacheability->setCacheMaxAge(30);
                  $response->setCache(['s_maxage' => 30, 'max_age' => 30]);
                  $response->addCacheableDependency($cacheability);
                  return $response;
                }
                $data_is_protected = TRUE;
              }
            }
            $values[$d_k][$t_key] = $entity;
          }
          elseif ($t_value instanceof TranslatableMarkup) {
            $options = $t_value->getOptions();
            $options['langcode'] = $langcode;
            $values[$d_k][$t_key] = $this->t($t_value->getUntranslatedString(), $t_value->getArguments(), $options);
          }
        }
      }
    }

    if (isset($values['content'])) {
      $build = MustacheRenderTemplate::build($values['name'], $values['content']);
    }
    else {
      $build = MustacheRenderTemplate::build($values['name']);
    }

    if (isset($values['url']) && (!is_string($values['url']) || strpos($values['url'], 'm/sync?') === FALSE)) {
      $build->usingDataFromUrl($values['url']);
    }
    elseif (!empty($values['data'])) {
      $build->usingData($values['data']);
    }

    if (isset($values['token_options'])) {
      $values['token_options']['langcode'] = $langcode;
      $build->withTokens($values['token_data'], $values['token_options']);
    }

    // Pass through any URL arguments as form values (except for the values
    // hash), which would then be passed to the URL endpoint, if given.
    $form_values = $request->query->all();
    unset($form_values['h']);
    if (!empty($form_values)) {
      $build->usingFormValues(['values' => $form_values]);
    }

    $build_render_array = &$build->toRenderArray();

    if (!empty($values['merge'])) {
      foreach ($values['merge'] as $m_key => $m_val) {
        if ($m_key === '#sync') {
          // Client-side synchronization is not happening here, as we are
          // literally synchronizing on the server-side.
          continue;
        }
        elseif (isset($build_render_array[$m_key]) && is_array($build_render_array[$m_key])) {
          $build_render_array[$m_key] = NestedArray::mergeDeep($build_render_array[$m_key], $m_val);
        }
        else {
          $build_render_array[$m_key] = $m_val;
        }
      }
    }

    // Also involve render caching for server-side synchronization.
    $build_render_array['#cache']['keys'] = ['mustache_sync'];
    $build_render_array['#cache']['contexts'][] = 'languages';
    // Refresh render results after a certain time threshold, when no other
    // criteria exists that could invalidate the cached result.
    if (!isset($build_render_array['#cache']['max-age']) || $build_render_array['#cache']['max-age'] === CacheBackendInterface::CACHE_PERMANENT) {
      $build_render_array['#cache']['max-age'] = $max_age;
    }
    $cacheability
      ->merge(CacheableMetadata::createFromRenderArray($build_render_array))
      ->applyTo($build_render_array);

    $renderer = $this->renderer;
    $rendered = $renderer->executeInRenderContext(new RenderContext(), function () use ($renderer, &$build_render_array) {
      return $renderer->render($build_render_array);
    });
    $cacheability
      ->addCacheableDependency(CacheableMetadata::createFromRenderArray($build_render_array));

    $result = ['synced' => $rendered, 'messages' => []];
    if ($all_messages = $this->messenger->all()) {
      $has_messages = FALSE;
      foreach ($all_messages as $type => $messages) {
        foreach ($messages as $message) {
          $result['messages'][] = [
            'status' => $type === MessengerInterface::TYPE_STATUS,
            'warning' => $type === MessengerInterface::TYPE_WARNING,
            'error' => $type === MessengerInterface::TYPE_ERROR,
            'text' => (string) $message,
          ];
          $has_messages = TRUE;
        }
      }
      if ($has_messages) {
        $cacheability->setCacheMaxAge(0);
        $this->messenger->deleteAll();
      }
    }

    $response = new CacheableJsonResponse($result, 200);
    $response->addCacheableDependency($cacheability);
    if ($data_is_protected || $cacheability->getCacheMaxAge() === 0) {
      $response->setPrivate();
    }
    else {
      $response->setPublic();
      // @todo Try to find a proper value or configurable settings for caching.
      $response->setSharedMaxAge(10);
    }

    return $response;
  }

}
