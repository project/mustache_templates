<?php

namespace Drupal\mustache_magic\StreamWrapper;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\LocalReadOnlyStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\mustache\Exception\MustacheTemplateNotFoundException;

/**
 * Mustache template stream wrapper ("mustache-template://template-name").
 *
 * This is a read-only stream.
 */
class MustacheTemplateStream extends LocalReadOnlyStream {

  /**
   * The finder of Mustache templates.
   *
   * @var \Drupal\mustache\MustacheTemplates
   */
  protected $templates;

  /**
   * The template storage.
   *
   * @var \Drupal\mustache_magic\Storage\MustacheTemplateStorage
   */
  protected $templateStorage;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The MustacheTemplateStream constructor.
   */
  public function __construct() {
    $this->templates = static::getTemplates();
    $this->templateStorage = static::getTemplateStorage();
    $this->streamWrapperManager = static::getStreamWrapperManager();
    $this->logger = static::getLogger();
  }

  /**
   * {@inheritdoc}
   */
  public static function getType() {
    return StreamWrapperInterface::HIDDEN | StreamWrapperInterface::READ;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return t('Mustache template contents');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('Read access to the contents of registered Mustache templates.');
  }

  /**
   * {@inheritdoc}
   */
  public function realpath() {
    return $this->getLocalPath();
  }

  /**
   * {@inheritdoc}
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    if ($template_name = $this->getTarget($uri)) {
      if (strpos($template_name, 'magic/') === 0) {
        list(, $hash) = explode('/', $template_name, 2);
        $template_name = NULL;

        if ($template = $this->templateStorage->get($hash)) {
          $values_hash = $this->templateStorage->hashValues($template);
          $temp_dir = 'temporary://mustache/magic';
          $temp_file = $temp_dir . '/' . $values_hash . '.mustache.tpl';
          if (!file_exists($temp_file)) {
            /** @var \Drupal\Core\File\FileSystemInterface $file_system */
            $file_system = \Drupal::service('file_system');
            if (!$file_system->prepareDirectory($temp_dir, FileSystemInterface::CREATE_DIRECTORY) || (!file_exists($temp_file) && FALSE === @file_put_contents($temp_file, $template['content']))) {
              $this->logger->error("Failed to write Mustache template into temporary directory.");
              return FALSE;
            }
          }
          return $this->streamWrapperManager->getViaUri($temp_file)->realpath();
        }
      }
      else {
        try {
          $filename = $this->templates->find($template_name);
        }
        catch (MustacheTemplateNotFoundException $e) {
          $this->logger->error($e->getMessage());
          return FALSE;
        }
        if ($wrapper = $this->streamWrapperManager->getViaUri($filename)) {
          if ($wrapper instanceof MustacheTemplateStream) {
            $this->logger->error(sprintf("Unresolved template file %s for Mustache template %s.", $filename, $template_name));
            return FALSE;
          }
          return $wrapper->realpath();
        }
        return realpath($filename);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    if (isset($this->uri) && ($realpath = $this->realpath())) {
      return dirname($realpath);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    if ($realpath = $this->realpath()) {
      return \Drupal::service('file_url_generator')->generateAbsoluteString($realpath);
    }
    return FALSE;
  }

  /**
   * Get the templates service.
   *
   * @return \Drupal\mustache\MustacheTemplates
   *   The templates service.
   */
  protected static function getTemplates() {
    return \Drupal::service('mustache.templates');
  }

  /**
   * Get the template storage.
   *
   * @return \Drupal\mustache_magic\Storage\MustacheTemplateStorage
   *   The template storage.
   */
  protected static function getTemplateStorage() {
    return \Drupal::service('mustache.template_storage');
  }

  /**
   * Get the stream wrapper manager.
   *
   * @return \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   *   The stream wrapper manager.
   */
  protected static function getStreamWrapperManager() {
    return \Drupal::service('stream_wrapper_manager');
  }

  /**
   * Get the logger channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger channel.
   */
  protected static function getLogger() {
    return \Drupal::service('logger.channel.mustache');
  }

}
