<?php

namespace Drupal\mustache_magic\Storage;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;

/**
 * Trait for expirable storages identifying values by using a hash key.
 */
trait ExpirableHashStorageTrait {

  /**
   * The key value store to use.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  public $kv;

  /**
   * The hash algorithm to use.
   *
   * @var string
   */
  public static $hashAlgo = 'sha3-512';

  /**
   * The hash salt to use (callable).
   *
   * @var string
   */
  public static $hashSalt = 'Drupal\\Core\\Site\\Settings::getHashSalt';

  /**
   * The expiry time in seconds.
   *
   * Default expiry is set for two weeks.
   *
   * @var int
   */
  public static $expire = 1209600;

  /**
   * Whether to refresh the expire when under a certain threshold.
   *
   * @var bool
   */
  public static $refreshExpire = TRUE;

  /**
   * Constructs a new ExpirableHashStorageTrait object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_factory
   *   The key value store to use.
   */
  public function __construct(KeyValueExpirableFactoryInterface $key_value_factory) {
    $this->kv = $key_value_factory->get(static::COLLECTION_NAME);
  }

  /**
   * Whether a record exists for the given hash.
   *
   * @param string $hash
   *   The hash key, usually generated with ::generateHash().
   *
   * @return bool
   *   Returns TRUE if it exists, FALSE otherwise.
   */
  public function has($hash) {
    return $this->kv->has($hash);
  }

  /**
   * Get the values that match to the given hash..
   *
   * @param string $hash
   *   The hash key, usually generated with ::generateHash().
   *
   * @return array|null
   *   The values array, or NULL if it not exists.
   */
  public function get($hash) {
    $values = $this->kv->get($hash);
    if ($values && static::$refreshExpire) {
      $threshold = \Drupal::time()->getRequestTime() + (int) (static::$expire / 2);
      if (!empty($values['expires']) && $values['expires'] < $threshold) {
        $this->set($values);
      }
    }
    return $values;
  }

  /**
   * Returns all stored key/value pairs in the collection.
   *
   * @return array
   *   An associative array containing all stored items in the collection.
   */
  public function getAll() {
    return $this->kv->getAll();
  }

  /**
   * Set the values.
   *
   * @param array $values
   *   The values to set.
   *
   * @return string
   *   The generated hash key for the set values.
   */
  public function set(array $values) {
    $hash = $this->generateHash($values);
    $values['expires'] = \Drupal::time()->getRequestTime() + static::$expire;
    $values['uid'] = \Drupal::currentUser()->id();
    $this->kv->setWithExpire($hash, $values, static::$expire);
    return $hash;
  }

  /**
   * Generates a unique hash for the given set of values.
   *
   * This method does not store anything in the key-value storage, it just
   * generated the hash for the given array.
   *
   * @param array $values
   *   The values.
   *
   * @return string
   *   The generated hash.
   */
  public function generateHash(array $values) {
    unset($values['expires'], $values['uid']);
    return hash(static::$hashAlgo, json_encode($values) . $this->getHashSalt());
  }

  /**
   * Get the hash salt.
   *
   * @return string
   *   The hash salt.
   */
  public function getHashSalt() {
    return call_user_func(static::$hashSalt);
  }

  /**
   * Deletes all entries in the storage.
   */
  public function deleteAll() {
    $this->kv->deleteAll();
  }

}
