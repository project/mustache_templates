<?php

namespace Drupal\mustache_magic\Storage;

/**
 * A storage of user-defined sync templates.
 */
class MustacheSyncStorage {

  /**
   * The key-value collection name.
   */
  const COLLECTION_NAME = 'mustache_sync';

  use ExpirableHashStorageTrait;

}
