<?php

namespace Drupal\mustache_magic\Storage;

/**
 * A storage for user-defined templates.
 */
class MustacheTemplateStorage {

  /**
   * The key-value collection name.
   */
  const COLLECTION_NAME = 'mustache_templates';

  use ExpirableHashStorageTrait {
    generateHash as hashValues;
  }

  /**
   * Registers a user-defined template.
   *
   * @param string $template_name
   *   The template name.
   * @param string $template_content
   *   The template content.
   *
   * @return bool
   *   Whether the template was new and added (TRUE) or if the template already
   *   exists in the storage, so that it was not necessary to perform a write.
   */
  public function registerTemplate($template_name, $template_content) {
    $template_values = [
      'name' => $template_name,
      'content' => $template_content,
    ];
    $template_storage_hash = $this->generateHash($template_values);
    $existing = $this->get($template_storage_hash);
    if (is_null($existing) || ($this->hashValues($existing) !== $this->hashValues($template_values))) {
      if ($template_storage_hash != $this->set($template_values)) {
        throw new \LogicException("The Mustache template storage is behaving unexpected: Received a different hash for given values other than previously generated.");
      }
      // Clearing all cached definitions is unfortunate, but required.
      mustache_cache_flush();
      if (\Drupal::service('mustache.summables')->isEnabled()) {
        \Drupal::service('mustache.summables')->clearCaches();
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function generateHash(array $values) {
    return hash(static::$hashAlgo, $values['name'] . $this->getHashSalt());
  }

}
