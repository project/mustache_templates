<?php

namespace Drupal\mustache_magic\Plugin\mustache\Magic;

use Drupal\mustache\Plugin\MustacheMagic;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Include Drupal asset libraries.
 *
 * Usage:
 *
 * You can use the "magical" {{#library.<lib1>+<lib2>+<lib3>+[...]}} variable to
 * include asset libraries.
 *
 * @code
 * {{#library.core/jquery+core/sortable}}
 * $('body');
 * {{/library.core/jquery+core/sortable}}
 * @endcode
 *
 * Asset libraries are defined via <extension>.libraries.yml files. Examples:
 * - https://api.drupal.org/api/drupal/core%21core.libraries.yml/9.3.x
 * - https://api.drupal.org/api/drupal/core%21modules%21media%21media.libraries.yml/9.3.x
 *
 * @MustacheMagic(
 *   id = "library",
 *   label = @Translation("Library inclusion"),
 *   description = @Translation("Use the {{#library.<b>&lt;lib1&gt;+&lt;lib2&gt;+&lt;lib3&gt;+[...]</b>}} variable to include asset libraries.<br/>Example:<p>{{#library.core/jquery+core/sortable}}<br/>$('body');<br/>{{/library.core/jquery+core/sortable}}</p><p>Asset libraries are defined via &lt;extension&gt;.libraries.yml files. Examples: <a href='https://api.drupal.org/api/drupal/core%21core.libraries.yml/9.3.x' target='_blank' rel='noopener noreferrer'>core.libraries.yml</a>, <a href='https://api.drupal.org/api/drupal/core%21modules%21media%21media.libraries.yml/9.3.x' target='_blank' rel='noopener noreferrer'>media.libraries.yml</a></p>"),
 * )
 */
class Library extends MustacheMagic {

  /**
   * The client library for library inclusion.
   *
   * @var string
   */
  public static $library = 'mustache_magic/magic.library';

  /**
   * A sequence of user-defined libraries to include.
   *
   * @var array
   */
  protected $libs = [];

  /**
   * Whether this object is a clone.
   *
   * @var bool
   */
  protected $cloned = FALSE;

  /**
   * The library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->libraryDiscovery = $container->get('library.discovery');
    return $instance;
  }

  /**
   * Implementation of the magic __isset() method.
   */
  public function __isset($name): bool {
    return !empty($name) && strpos($name, '/') !== FALSE;
  }

  /**
   * Implementation of the magic __get() method.
   */
  public function __get($name) {
    if (!$this->cloned) {
      $cloned = clone $this;
      $cloned->cloned = TRUE;
      return $cloned->__get($name);
    }
    $this->libs = array_merge($this->libs, explode('+', str_replace('_dot_', '.', mb_strtolower(trim($name)))));
    return $this;
  }

  /**
   * Implements the magic __invoke() method, used as higher-order section.
   */
  public function __invoke($template_content = NULL, $render = NULL) {
    if (!isset($template_content, $render)) {
      return;
    }

    $this->attachLibrary();
    if (empty($this->element['#attached']['library'])) {
      $this->element['#attached']['library'] = [];
    }
    $attached = &$this->element['#attached']['library'];
    foreach ($this->libs as $lib) {
      if (!in_array($lib, $attached) && strpos($lib, '/') !== FALSE) {
        list($extension, $name) = explode('/', $lib, 2);
        if ($this->libraryDiscovery->getLibraryByName($extension, $name)) {
          $attached[] = $lib;
        }
        else {
          $this->logger->warning(sprintf("The user-defined asset library %s does not exist.", $lib));
        }
      }
    }
    return $render($template_content);
  }

}
