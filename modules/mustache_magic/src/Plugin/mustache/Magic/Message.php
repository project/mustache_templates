<?php

namespace Drupal\mustache_magic\Plugin\mustache\Magic;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\mustache\Plugin\MustacheMagic;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Display a message to the user.
 *
 * Usage:
 *
 * Use the {{#message.<type>}} section variable to display a message.
 *
 * @code
 *  {{#message.status}}This is a status message.{{/message.status}}
 *  {{#message.warning}}This is a warning message.{{/message.warning}}
 *  {{#message.error}}This is an error message.{{/message.error}}
 * @endcode
 *
 * @MustacheMagic(
 *   id = "message",
 *   label = @Translation("Message"),
 *   description = @Translation("Use the <b>{{#message.&lt;type&gt;}}</b> section variable to display a message to the user. Examples: <ul><li>{{#message.status}}This is a status message.{{/message.status}}</li><li>{{#message.warning}}This is a warning message.{{/message.warning}}</li><li>{{#message.error}}This is an error message.{{/message.error}}</li></ul>")
 * )
 */
class Message extends MustacheMagic {

  /**
   * Whether this object is a clone.
   *
   * @var bool
   */
  protected $cloned = FALSE;

  /**
   * The client library for displaying messages.
   *
   * @var string
   */
  public static $library = 'mustache_magic/magic.message';

  /**
   * A list of supported message types.
   *
   * @var array
   */
  public static $messageTypes = [
    'status' => MessengerInterface::TYPE_STATUS,
    'warning' => MessengerInterface::TYPE_WARNING,
    'error' => MessengerInterface::TYPE_ERROR,
  ];

  /**
   * The selected message type.
   *
   * @var string
   */
  protected $messageType = 'status';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    if (!isset($instance->messenger)) {
      $instance->messenger = $container->get('messenger');
    }
    return $instance;
  }

  /**
   * Implementation of the magic __isset() method.
   */
  public function __isset($name): bool {
    return !empty($name) && isset(static::$messageTypes[mb_strtolower(trim($name))]);
  }

  /**
   * Implementation of the magic __get() method.
   */
  public function __get($name) {
    if (!$this->cloned) {
      $cloned = clone $this;
      $cloned->cloned = TRUE;
      return $cloned->__get($name);
    }
    $name = mb_strtolower(trim($name));
    if (isset(static::$messageTypes[$name])) {
      $this->messageType = $name;
    }
    return $this;
  }

  /**
   * Implements the magic __invoke() method, used as higher-order section.
   */
  public function __invoke($template_content = NULL, $render = NULL) {
    if (!isset($template_content, $render)) {
      return;
    }

    $this->attachLibrary();
    $this->element['#cache']['max-age'] = 0;
    $rendered = trim($render($template_content));
    if (!empty($rendered)) {
      $this->messenger->addMessage($rendered, static::$messageTypes[$this->messageType]);
    }
    return "";
  }

}
