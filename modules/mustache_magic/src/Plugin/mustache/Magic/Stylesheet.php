<?php

namespace Drupal\mustache_magic\Plugin\mustache\Magic;

use Drupal\Component\Utility\Html;

/**
 * Write aggregateable CSS (Cascading Style Sheets).
 *
 * Usage:
 *
 * Use the {{#css.<name>}} section variable to define aggregateable CSS.
 * You can refer to this aggregated asset using {{><name>.css}}.
 *
 * @code
 *  {{#css.bluetint}}body {color: blue;}{{/css.bluetint}}
 *  // You may reuse this asset like this:
 * {{>bluetint.css}}
 * @endcode
 *
 * Technically, this plugin is implemented using a "CSS in JS" approach. It is
 * being loaded as Mustache template within the client, and this template then
 * executes JS logic that in turn attaches the stylesheet into the DOM.
 *
 * @MustacheMagic(
 *   id = "css",
 *   label = @Translation("CSS (Cascading Style Sheet)"),
 *   description = @Translation("Use the {{#css.<b>&lt;name&gt;</b>}} section variable to define aggregateable CSS. You can refer to this aggregated asset using {{&gt;<b>&lt;name&gt;</b>.css}}. Example:<p>{{#css.bluetint}}body {color: blue;}{{/css.bluetint}}<br/>// You may reuse this asset like this:<br/>{{&gt;bluetint.css}}</p>")
 * )
 */
class Stylesheet extends Javascript {

  /**
   * The asset type extension.
   *
   * @var string
   */
  public static $assetType = 'css';

  /**
   * {@inheritdoc}
   */
  protected function templateData($name, $content) {
    return [
      'id' => Html::getUniqueId($name),
      'name' => $name,
      'encoded' => trim(substr(json_encode($content, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT), 1, -1)),
    ];
  }

}
