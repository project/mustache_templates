<?php

namespace Drupal\mustache_token;

use Drupal\Core\Utility\Token;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\mustache\Helpers\Mustache;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache\Render\Markup;

/**
 * A trait for Mustache-specific token service decorators.
 */
trait MustacheTokenDecoratorTrait {

  /**
   * The decorated token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The renderer, usually injected via constructor.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Set the token service that is being decorated by this service.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service to decorate.
   */
  public function setDecoratedToken(Token $token) {
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function replace($text, array $data = [], array $options = [], BubbleableMetadata $bubbleable_metadata = NULL) {
    if (empty($text) || !Mustache::containsSyntax($text)) {
      // When no Mustache template syntax is included, then there is nothing to
      // do for this decorator.
      return $this->token->replace($text, $data, $options, $bubbleable_metadata);
    }

    $bubbleable_metadata_is_passed_in = (bool) $bubbleable_metadata;
    $bubbleable_metadata = $bubbleable_metadata ?: new BubbleableMetadata();

    $template = MustacheRenderTemplate::build(hash('md4', $text), $text)
      ->withTokens($data, $options);
    $build = $template->toRenderArray();

    if (!$bubbleable_metadata_is_passed_in && $this->renderer->hasRenderContext()) {
      $markup = $this->renderer->render($build);
    }
    else {
      $markup = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$build) {
        return $this->renderer->render($build);
      });
      // Yep, this is necessary because ::merge() clones the object...
      $new_bubble = $bubbleable_metadata->merge(BubbleableMetadata::createFromRenderArray($build));
      $bubbleable_metadata->setAttachments($new_bubble->getAttachments());
      $bubbleable_metadata->setCacheContexts($new_bubble->getCacheContexts());
      $bubbleable_metadata->setCacheMaxAge($new_bubble->getCacheMaxAge());
      $bubbleable_metadata->setCacheTags($new_bubble->getCacheTags());
    }

    return empty($build['#use_sync']) && isset($build['#content']) ? Markup::create(trim((string) $build['#content'])) : Markup::create(trim((string) $markup));
  }

  /**
   * {@inheritdoc}
   */
  public function scan($text) {
    return $this->token->scan($text);
  }

  /**
   * {@inheritdoc}
   */
  public function generate($type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
    return $this->token->generate($type, $tokens, $data, $options, $bubbleable_metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function findWithPrefix(array $tokens, $prefix, $delimiter = ':') {
    return $this->token->findWithPrefix($tokens, $prefix, $delimiter);
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return $this->token->getInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function setInfo(array $tokens) {
    $this->token->setInfo($tokens);
  }

  /**
   * {@inheritdoc}
   */
  public function resetInfo() {
    $this->token->resetInfo();
  }

}
