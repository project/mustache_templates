<?php

namespace Drupal\mustache_token;

use Drupal\Core\Utility\Token;

/**
 * The Mustache token service, that is a decorator of the core token service.
 */
class MustacheCoreToken extends Token {

  use MustacheTokenDecoratorTrait;

}
