<?php

namespace Drupal\mustache_token;

use Drupal\token\Token;
use Drupal\token\TokenInterface as ContribTokenInterface;

/**
 * The Mustache token service, that is a decorator of the contrib token service.
 */
class MustacheContribToken extends Token implements ContribTokenInterface {

  use MustacheTokenDecoratorTrait;

  /**
   * The decorated token service.
   *
   * @var \Drupal\token\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function getTypeInfo($token_type) {
    return $this->token->getTypeInfo($token_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenInfo($token_type, $token) {
    return $this->token->getTokenInfo($token_type, $token);
  }

  /**
   * {@inheritdoc}
   */
  public function getGlobalTokenTypes() {
    return $this->token->getGlobalTokenTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function getInvalidTokens($type, $tokens) {
    return $this->token->getInvalidTokens($type, $tokens);
  }

  /**
   * {@inheritdoc}
   */
  public function getInvalidTokensByContext($value, array $valid_types = []) {
    return $this->token->getInvalidTokensByContext($value, $valid_types);
  }

}
