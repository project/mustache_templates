<?php

namespace Drupal\mustache_token;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Provider for dynamically provided services by the mustache_token module.
 */
class MustacheTokenServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if (class_exists('Drupal\token\Token')) {
      // Replace the core decorator with the contrib decorator.
      $definition = $container->getDefinition('mustache.token');
      $definition->setClass('Drupal\mustache_token\MustacheContribToken');
    }
  }

}
