<?php

namespace Drupal\Tests\mustache_token\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;

/**
 * Kernel tests for Mustache Logic-less tokens.
 *
 * @group mustache
 * @group mustache_token
 */
class MustacheTokenTest extends KernelTestBase {

  protected static $modules = [
    'system',
    'user',
    'field',
    'text',
    'mustache',
    'mustache_token',
    'token',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installConfig(static::$modules);

    // Create the single-value string field, using inlimited cardinality.
    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_string_single',
      'type' => 'string',
      'entity_type' => 'user',
      'settings' => [
        'target_type' => 'user',
      ],
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
    ]);
    $field_definition->save();
    $field = FieldConfig::create([
      'field_storage' => $field_definition,
      'label' => 'Single-string field.',
      'entity_type' => 'user',
      'bundle' => 'user',
    ]);
    $field->save();

    // Create the multi-value string field, using inlimited cardinality.
    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_string_multi',
      'type' => 'string',
      'entity_type' => 'user',
      'settings' => [
        'target_type' => 'user',
      ],
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
    ]);
    $field_definition->save();
    $field = FieldConfig::create([
      'field_storage' => $field_definition,
      'label' => 'Multi-string field.',
      'entity_type' => 'user',
      'bundle' => 'user',
    ]);
    $field->save();

    User::create([
      'uid' => 0,
      'name' => 'anonymous'])->save();
    User::create([
      'uid' => 1,
      'name' => 'admin',
      'field_string_single' => 'A single string value...',
      'field_string_multi' => ['One', 'Two', 'Three', 'Four', '!Five!'],
    ])->save();
  }

  /**
   * Tests iterating through Tokens.
   */
  public function testIterate() {
    $admin_user = User::load(1);
    $token = \Drupal::token();
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    $template = "{{#iterate.loop.user.field_string_single}}START: {{#first}}FIRST {{/first}}{{^first}}NOT-FIRST!:{{/first}}{{position}}. {{.}}{{#last}} - LAST!{{/last}} END{{/iterate.loop.user.field_string_single}}";
    $this->assertEquals('', $token->replace($template, ['user' => $admin_user]), 'User without permissions must not have access to print out values.');

    $account_switcher->switchTo($admin_user);
    $this->assertEquals('START: FIRST 1. A single string value... - LAST! END', $token->replace($template, ['user' => $admin_user]));

    $account_switcher->switchBack();

    $template = "START: {{#iterate.loop.user.field_string_multi}}{{#first}}FIRST {{/first}} {{position}}. {{.}}{{^last}}, {{/last}}{{#last}} - LAST!{{/last}}{{/iterate.loop.user.field_string_multi}} END";
    $this->assertEquals('START:  END' , $token->replace($template, ['user' => $admin_user]));

    $account_switcher->switchTo($admin_user);
    $this->assertEquals('START: FIRST  1. One,  2. Two,  3. Three,  4. Four,  5. !Five! - LAST! END', $token->replace($template, ['user' => $admin_user]));

    $template = "START: {{#iterate.explode.user.field_string_multi}}{{#first}}FIRST {{/first}} {{position}}. {{.}}{{^last}}, {{/last}}{{#last}} - LAST!{{/last}}{{/iterate.explode.user.field_string_multi}} END";
    $this->assertEquals('START: FIRST  1. One,  2. Two,  3. Three,  4. Four,  5. !Five! - LAST! END', $token->replace($template, ['user' => $admin_user]));

    $template = "{{iterate.count.user.field_string_single}}";
    $this->assertSame('1', (string) $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.count.user.field_string_multi}}";
    $this->assertSame('5', (string) $token->replace($template, ['user' => $admin_user]));

    $template = "{{iterate.first.user.field_string_single}}";
    $this->assertEquals('A single string value...', $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.first.user.field_string_multi}}";
    $this->assertEquals('One', $token->replace($template, ['user' => $admin_user]));

    $template = "{{iterate.last.user.field_string_single}}";
    $this->assertEquals('A single string value...', $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.last.user.field_string_multi}}";
    $this->assertEquals('!Five!', $token->replace($template, ['user' => $admin_user]));

    $template = "{{iterate.min.user.field_string_single}}";
    $this->assertSame('24', (string) $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.min.user.field_string_multi}}";
    $this->assertEquals('3', $token->replace($template, ['user' => $admin_user]));

    $template = "{{iterate.max.user.field_string_single}}";
    $this->assertSame('24', (string) $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.max.user.field_string_multi}}";
    $this->assertEquals('6', $token->replace($template, ['user' => $admin_user]));

    $admin_user->get('field_string_multi')->setValue(['3', '5', '12']);
    $admin_user->save();
    $admin_user = User::load(1);

    $template = "{{iterate.min.user.field_string_multi}}!";
    $this->assertEquals('3!', $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.max.user.field_string_multi}}!";
    $this->assertEquals('12!', $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.sum.user.field_string_multi}}!";
    $this->assertEquals('20!', $token->replace($template, ['user' => $admin_user]));
    $template = "{{iterate.avg.user.field_string_multi}}!";
    $this->assertEquals('6.67!', $token->replace($template, ['user' => $admin_user]));

    $account_switcher->switchBack();
  }

  /**
   * Tests magic conditions using tokens.
   */
  public function testMagicConditions() {
    $admin_user = User::load(1);
    $token = \Drupal::token();
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');

    $template = "{{#if.user.empty}}EMPTY!{{/if.user.empty}}{{^if.user.empty}}NOT EMPTY!{{/if.user.empty}}";
    $this->assertEquals('EMPTY!', $token->replace($template, []));

    $template = "{{#if.user.uid.numeric}}IS NUMERIC!{{/if.user.uid.numeric}}{{^if.user.uid.numeric}}NOT NUMERIC!{{/if.user.uid.numeric}}";
    $this->assertEquals('NOT NUMERIC!', $token->replace($template, []), "No access to view the user.");

    $template = "{{#if.user.uid.greaterthan.0}}GREATER{{/if.user.uid.greaterthan.0}}{{^if.user.uid.greaterthan.0}}NOT GREATER!{{/if.user.uid.greaterthan.0}}";
    $this->assertEquals('NOT GREATER!', $token->replace($template, []));

    $account_switcher->switchTo($admin_user);

    $template = "{{#if.user.empty}}EMPTY!{{/if.user.empty}}{{^if.user.empty}}NOT EMPTY!{{/if.user.empty}}";
    $this->assertEquals('NOT EMPTY!', $token->replace($template, ['user' => $admin_user]));
    $template = "{{#if.user.empty}}EMPTY!{{/if.user.empty}}{{^if.user.empty}}{{user.uid}}{{/if.user.empty}}";
    $this->assertSame('1', (string) $token->replace($template, ['user' => $admin_user]));

    $template = "{{#if.user.uid.numeric}}IS NUMERIC!!{{/if.user.uid.numeric}}{{^if.user.uid.numeric}}NOT NUMERIC!!{{/if.user.uid.numeric}}";
    $this->assertEquals('IS NUMERIC!!', $token->replace($template, []));
    $this->assertEquals('IS NUMERIC!!', $token->replace($template, ['user' => $admin_user]));

    $template = "{{#if.user.uid.greaterthan.0}}GREATER{{/if.user.uid.greaterthan.0}}{{^if.user.uid.greaterthan.0}}NOT GREATER!{{/if.user.uid.greaterthan.0}}";
    $this->assertEquals('GREATER', $token->replace($template, ['user' => $admin_user]));
    $this->assertEquals('GREATER', $token->replace($template, []), "Current user is admin, and admin user has UID 1, which is greater than 0");
    $this->assertEquals('NOT GREATER!', $token->replace($template, ['user' => User::load(0)]));

    $template = "{{#if.user.uid.lessthan.10}}LESS{{/if.user.uid.lessthan.10}}{{^if.user.uid.lessthan.10}}NOT LESS!{{/if.user.uid.lessthan.10}}";
    $this->assertEquals('LESS', $token->replace($template, ['user' => $admin_user]));
    $this->assertEquals('LESS', $token->replace($template, []));

    $template = "{{#if.user.field_string_multi.atleast.5}}AT LEAST!{{/if.user.field_string_multi.atleast.5}}{{#if.user.field_string_multi.atleast.10}}No.{{/if.user.field_string_multi.atleast.10}}";
    $this->assertEquals('AT LEAST!', $token->replace($template, ['user' => $admin_user]));
    $this->assertEquals('AT LEAST!', $token->replace($template, []));

    $template = "{{#if.user.field_string_multi.atmost.5}}AT MOST!{{/if.user.field_string_multi.atmost.5}}{{#if.user.field_string_multi.atmost.3}}No.{{/if.user.field_string_multi.atmost.3}}";
    $this->assertEquals('AT MOST!', $token->replace($template, ['user' => $admin_user]));
    $this->assertEquals('AT MOST!', $token->replace($template, []));

    $template = "{{#if.user.field_string_multi.biggerthan.4}}MORE!{{/if.user.field_string_multi.biggerthan.4}}{{#if.user.field_string_multi.biggerthan.30}}No.{{/if.user.field_string_multi.biggerthan.30}}";
    $this->assertEquals('MORE!', $token->replace($template, ['user' => $admin_user]));

    $template = "{{#if.user.field_string_multi.lessthan.6}}LESS!{{/if.user.field_string_multi.lessthan.6}}{{#if.user.field_string_multi.lessthan.5}}No.{{/if.user.field_string_multi.lessthan.5}}";
    $this->assertEquals('LESS!', $token->replace($template, ['user' => $admin_user]));

    $template = "{{#if.user.field_string_multi.0.value.equals.One}}EQUALS!{{/if.user.field_string_multi.0.value.equals.One}}";
    $this->assertEquals('EQUALS!', $token->replace($template, ['user' => $admin_user]));
    $template = "{{#if.user.field_string_multi.0.value.equals.Two}}EQUALS!{{/if.user.field_string_multi.0.value.equals.Two}}";
    $this->assertEquals('', $token->replace($template, ['user' => $admin_user]));

    $account_switcher->switchBack();
  }

}
