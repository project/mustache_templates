<h1>The {{foo}} details</h1>
<div><b>Sequence:</b> {{sequence}}</div>
<div>{{^if.sequence.empty}}This sequence is not empty.{{/if.sequence.empty}}</div>
<div>{{#if.sequence.numeric}}This sequence is numeric.{{/if.sequence.numeric}}</div>
<div>{{#if.sequence.greaterthan.1000}}This sequence is greater than 1000!{{/if.sequence.greaterthan.1000}}</div>
<div>{{^if.sequence.lessthan.1000}}This sequence is not less than 1000!{{/if.sequence.lessthan.1000}}</div>
{{#previous}}
  <div><b>Previous sequence:</b> {{previous.sequence}}</div>
  <div>{{^if.previous.sequence.empty}}The previous sequence is also not empty.{{/if.previous.sequence.empty}}</div>
  <div>{{#if.previous.sequence.numeric}}The previous sequence is also numeric.{{/if.previous.sequence.numeric}}</div>
  <div>{{#if.previous.sequence.greaterthan.1000}}The previous sequence is greater than 1000!{{/if.previous.sequence.greaterthan.1000}}</div>
  <div>{{^if.previous.sequence.lessthan.1000}}The previous sequence is not less than 1000!{{/if.previous.sequence.lessthan.1000}}</div>
  <div>{{#if.sequence.greaterthan.previous}}The new sequence is greather than the previous one.{{/if.sequence.greaterthan.previous}}</div>
  <div>{{^if.sequence.lessthan.previous}}The new sequence is not less than the previous one.{{/if.sequence.lessthan.previous}}</div>
  <div>{{^if.sequence.equals.previous}}The new sequence is not equal to the previous one.{{/if.sequence.equals.previous}}</div>
{{/previous}}
