<h1>The {{foo}} details</h1>
{{#previous.id}}
<div><b>Previous ID:</b> {{previous.id}}</div>
{{/previous.id}}
<div><b>New ID:</b> {{id}}</div>
<div><b>Number:</b> {{number}}</div>
{{#nested}}
<div><b>Nested entry:</b> {{key}}</div>
{{/nested}}
