<b>{{info}}</b><br/>
<b>Generated value: {{value}}</b>
<style>
  .mustache-sync {
    transition: opacity .5s ease-in-out;
    -moz-transition: opacity .5s ease-in-out;
    -ms-transition: opacity .5s ease-in-out;
    -o-transition: opacity .5s ease-in-out;
    color: green;
  }
  .syncing {
    opacity: 0;
  }
  .synced {
    opacity: 1;
  }
</style>