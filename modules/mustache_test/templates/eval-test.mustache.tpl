<div class="mustache-test-change-me">I am original content.</div>
<script async src="/mustache-test/javascript.js"></script>
<script type="text/javascript">console.log('This got executed from inline JS, everytime you render this template.');</script>
<script id="my-inline-script">console.log('This got executed inline, but only once because it has an element ID.');</script>
