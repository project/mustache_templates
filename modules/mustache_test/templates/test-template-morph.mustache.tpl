<h2 class="static-headline">This headline would not change, thus not being replaced in the DOM.</h2>
<div><b>ID:</b> {{id}}</div>
<div id="some-static-part">This line is static and will not change.</div>
<div><b>Number:</b> {{number}}</div>
{{#nested}}
<div><b>Nested entry:</b> {{key}}</div>
{{/nested}}
