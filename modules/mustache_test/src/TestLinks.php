<?php

namespace Drupal\mustache_test;

use Drupal\mustache\Render\Markup;

/**
 * Helper class to get a HTML list of test links.
 */
abstract class TestLinks {

  /**
   * Get a HTML list of test links.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The markup containing test links.
   */
  public static function get() {
    $output = '<h3>Test links:</h3>';
    $output .= '<ul>';
    $output .= '<li><a href="/mustache-test/period">Periodically refreshes (/period)</a></li>';
    $output .= '<li><a href="/mustache-test/interactive">Interactive (/interactive)</a></li>';
    $output .= '<li><a href="/mustache-test/interactive-previous">Interactive previous (/interactive-previous)</a></li>';
    $output .= '<li><a href="/mustache-test/interactive-adjacent">Interactive adjacent (/interactive-adjacent)</a></li>';
    $output .= '<li><a href="/mustache-test/eval">Inner script execution (/eval)</a></li>';
    $output .= '<li><a href="/mustache-test/trigger-placeholder">Placeholder with triggering element (/trigger-placeholder)</a></li>';
    $output .= '<li><a href="/mustache-test/eval-inline">Inline templates with inner script execution (/eval-inline)</a></li>';
    $output .= '<li><a href="/mustache-test/limited-trigger-placeholder">Placeholder with triggering element (/limited-trigger-placeholder)</a></li>';
    $output .= '<li><a href="/mustache-test/switchables">Switchables (/switchables)</a></li>';
    $output .= '<li><a href="/mustache-test/switchables-partial">Switchables (using partials, /switchables-partial)</a></li>';
    $output .= '<li><a href="/mustache-test/switchables-partial-inline">Switchables (using inline partials, /switchables-partial-inline)</a></li>';
    $output .= '<li><a href="/mustache-test/auto-increment">Increments (/auto-increment)</a></li>';
    $output .= '<li><a href="/mustache-test/auto-increment-interactive">Increments (/auto-increment-interactive)</a></li>';
    $output .= '<li><a href="/mustache-test/client-echo">Echo form (/client-echo)</a></li>';
    $output .= '<li><a href="/mustache-test/post-form">POST form (/post-form)</a></li>';
    $output .= '<li><a href="/mustache-test/token">Inline template with Tokens (/token)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-conditions">Magic conditions (/magic-conditions)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-sync">Magic synchronization (/magic-sync)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-sync-inline-tokens">Magic synchronization (inline, with Tokens, /magic-sync-inline-tokens)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-sync-inline-tokens-limited">Magic synchronization (inline, limited, /magic-sync-inline-tokens-limited)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-sync-inline-tokens-interactive">Magic synchronization (inline, interactive, /magic-sync-inline-tokens-interactive)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-sync-somewhere-else">Magic synchronization somewhere else (inline, /magic-sync-somewhere-else)</a></li>';
    $output .= '<li><a href="/mustache-test/magic-filters">Magic filters (/magic-filters)</a></li>';
    $output .= '<li><a href="/mustache-test/not-modified">Not modified (/not-modified)</a></li>';
    $output .= '</ul>';
    return Markup::create($output);
  }

}
