<?php

namespace Drupal\mustache_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache_test\TestLinks;

/**
 * A simple test form to be used at form binding tests.
 */
class MustacheTestForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mustache_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // When user input is empty, it means that the form is being initially
    // rendered. When it is rebuilding, it was submitted. For any other case,
    // e.g. when the submission handling calls this method, the Mustache render
    // array should not be built, as it would make an unnecessary request to the
    // Json endpoint, but the output will be rebuild anyway because
    // $form_state->setRebuild() is being called in the submit callback.
    if (empty($form_state->getUserInput()) || $form_state->isRebuilding()) {
      $build = MustacheRenderTemplate::build('test_form_values');
      $build->usingDataFromUrl('/mustache-test/echo-feed')
        ->usingFormValues($form_state)
        ->withClientSynchronization()
        ->morphing()
        ->startsWhenElementWasTriggered('.bind-value')
        ->atEvent('input')
        ->always();
      $form['template'] = $build->toRenderArray();
      $form['template']['#weight'] = 0;
    }
    $form['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search:'),
      '#default_value' => $form_state->hasValue('search') ? $form_state->getValue('search') : '',
      '#weight' => 10,
      '#attributes' => ['class' => ['bind-value']],
    ];
    $form['uppercase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Uppercase'),
      '#default_value' => FALSE,
      '#weight' => 20,
      '#attributes' => ['class' => ['bind-value']],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 30,
    ];
    $form['links'] = [
      '#markup' => TestLinks::get(),
      '#weight' => 40,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    $form_state->disableRedirect();
  }

}
