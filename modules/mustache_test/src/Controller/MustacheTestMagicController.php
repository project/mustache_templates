<?php

namespace Drupal\mustache_test\Controller;

use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache\Render\Markup;
use Drupal\mustache_test\TestLinks;

/**
 * A controller for testing magic variable plugins.
 */
class MustacheTestMagicController {

  /**
   * Returns a plain page for testing magic sync.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageSync() {
    $build = MustacheRenderTemplate::build('test_magic_sync');
    $build->usingDataFromUrl('/mustache-test/cacheable-json');

    $render = [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for testing inline magic sync using Tokens.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageSyncInlineTokens() {
    $build = MustacheRenderTemplate::build('my_magic_inline_tokens', "{{#sync.now.1.always.summable}}<div>Site name: {{#if.site.name.defined}}{{site.name}}{{/if.site.name.defined}}</div><div>Random hash: {{random.hash.md5}}</div>{{/sync.now.1.always.summable}}");
    $build->withTokens();

    $render = [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Testing inline magic sync using Tokens with limited increments.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageSyncInlineTokensLimited() {
    $build = MustacheRenderTemplate::build('my_magic_inline_tokens_limited', "{{#sync.now.1.10.span}}Site name: {{site.name}}<br />Random hash: {{random.hash.sha1}}{{/sync.now.1.10.span}}");
    $build->withTokens();

    $render = [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for testing inline magic sync using Tokens.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageSyncInlineTokensInteractive() {
    $build = MustacheRenderTemplate::build('my_magic_inline_tokens_interactive', "{{#sync.trigger.click-me.click}}<div>Site name: {{site.name}}</div><div>Random hash: {{random.hash.crc32b}}</div>{{/sync.trigger.click-me.click}}");
    $build->withTokens();

    $render = [
      $build->toRenderArray(),
      [
        '#type' => 'markup',
        '#markup' => Markup::create('<button type="button" class="click-me">Click here to refresh!</button>'),
      ],
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for testing inline magic sync using Tokens.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageSyncIntoSomewhereElse() {
    $build = MustacheRenderTemplate::build('magic_into_somewhere_else', "
    {{#sync.2.10.always.into.head>title}}SYNCED: {{site.name}}{{/sync.2.10.always.into.head>title}}
    {{#sync.now.url./mustache-test/json.into.another-target}}
      Replaced using ID <b>{{id}}</b>.
    {{/sync.now.url./mustache-test/json.into.another-target}}
    ");
    $build->withTokens();

    $render = [
      $build->toRenderArray(),
      [
        '#type' => 'markup',
        '#markup' => Markup::create('<div class="another-target">Please replace me...</div>'),
      ],
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for testing magic filters.
   *
   * @return array
   *   A build array for rendering the Mustache template(s).
   */
  public function pageFilters() {
    $render_array = [];

    $render_array[] = MustacheRenderTemplate::build('filter_template_1', 'filter.trim: {{#filter.trim}}          Hello!       {{/filter.trim}}')
    ->withClientSynchronization()
    ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_2', 'filter.striptags: {{#filter.striptags}}<h1><strong>Hello</strong> <span>there!</span></h1>{{/filter.striptags}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_3', "filter.nl2br: {{#filter.nl2br}}Hello\nthere!{{/filter.nl2br}}")
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_4', 'filter.spaceless: {{#filter.spaceless}}<p> <b>Hello there! </b></p>{{/filter.spaceless}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_5', 'filter.upper: {{#filter.upper}}Hello!{{/filter.upper}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_6', 'filter.lower: {{#filter.lower}}Hello!{{/filter.lower}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_7', 'filter.capitalize: {{#filter.capitalize}}hello!{{/filter.capitalize}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_8', 'filter.truncate (no change): {{#filter.truncate}}aaaaa bbbbbbb ccccccccc dddddddd eeee fffffffff ggggg{{/filter.truncate}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_9', 'filter.truncate.30: {{#filter.truncate.30}}aaaaa bbbbbbb ccccccccc dddddddd eeee fffffffff ggggg{{/filter.truncate.30}}')
      ->withClientSynchronization()
      ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];

    $render_array[] = MustacheRenderTemplate::build('filter_template_10', 'filter.path: {{#filter.path}}Hello There/GoodBye{{/filter.path}}')
      ->withClientSynchronization()
      ->toRenderArray();

      $render_array[] = ['#markup' => Markup::create('<hr/>')];

      $render_array[] = MustacheRenderTemplate::build('filter_template_11', 'filter.plus: 1 + 10 = {{#filter.plus.10}}1{{/filter.plus.10}}')
        ->withClientSynchronization()
        ->toRenderArray();

      $render_array[] = ['#markup' => Markup::create('<hr/>')];

      $render_array[] = MustacheRenderTemplate::build('filter_template_12', 'filter.minus: 1 - 1 = {{#filter.minus.1}}1{{/filter.minus.1}}')
        ->withClientSynchronization()
        ->toRenderArray();

    $render_array[] = ['#markup' => Markup::create('<hr/>')];
    $render_array[] = ['#markup' => TestLinks::get()];

    return $render_array;
  }

}
