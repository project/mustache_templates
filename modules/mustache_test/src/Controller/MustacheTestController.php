<?php

namespace Drupal\mustache_test\Controller;

use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache_test\TestLinks;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * A controller delivering resources for testing Mustache templates.
 */
class MustacheTestController {

  /**
   * Returns a Json response containing dummy data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   */
  public function jsonFeed() {
    $data = [
      'foo' => 'bar',
      'id' => uniqid(),
      'number' => rand(),
      'nested' => ['key' => rand()],
    ];
    $response = new JsonResponse();
    $response->setPrivate();
    $response->setJson(json_encode($data));
    return $response;
  }

  /**
   * Returns a response with Javascript code.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response containing Javascript code.
   */
  public function javascriptProvider() {
    $js_code = <<<JS
console.log("I got embedded.");
var elements = document.querySelectorAll('.mustache-test-change-me');
for (var i = 0; i < elements.length; i++) {
  elements[i].innerHTML = 'This content got inserted by embedded JS code.';
}
JS;
    $response = new Response($js_code);
    $response->headers->set('Content-Type', 'application/javascript');
    return $response;
  }

  /**
   * Returns a plain page for testing inner script execution.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageEval() {
    $build = MustacheRenderTemplate::build('test_eval');
    $placeholder = ['#markup' => ''];
    $build->withPlaceholder($placeholder);
    $build->withClientSynchronization()
      ->executesInnerScripts(TRUE)
      ->startsWhenElementWasTriggered('.click-me')
      ->atEvent('click')
      ->always();
    return [
      $build->toRenderArray(),
      ['#markup' => '<div class="click-me">Click me!</div>'],
      ['#markup' => TestLinks::get()],
    ];
  }

  /**
   * Returns a plain page for testing a certain template with period.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pagePeriod() {
    $build = MustacheRenderTemplate::build('some_test_template');
    $build->usingDataFromUrl('/mustache-test/json');
    $build->exposeData();
    $build->withClientSynchronization()
      ->startsDelayed(500)
      ->periodicallyRefreshesAt(1000);
    return [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
  }

  /**
   * Returns a plain page for interactively testing a certain template.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageInteractive() {
    $build = MustacheRenderTemplate::build('some_test_template');
    $build->usingDataFromUrl('/mustache-test/json');
    $build->withClientSynchronization()
      ->startsWhenElementWasTriggered('.click-me')
      ->atEvent('click')
      ->always();

    $render = [
      $build->toRenderArray(),
      [
        '#type' => 'markup',
        '#markup' => '<div class="click-me">Click here to refresh!</div>',
      ],
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for interactively appending a certain template.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageInteractiveAdjacent() {
    $build = MustacheRenderTemplate::build('some_test_template');
    $build->usingDataFromUrl('/mustache-test/json');
    $build->withClientSynchronization()
      ->insertsAt('afterbegin')
      ->executesInnerScripts(TRUE)
      ->startsDelayed(200)
      ->startsWhenElementWasTriggered('.click-me')
      ->atEvent('click')
      ->always();

    $render = [
      $build->toRenderArray(),
      [
        '#type' => 'markup',
        '#markup' => '<div class="click-me">Click here to add another one!</div>',
      ],
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

  /**
   * Returns a plain page for testing a certain template with a triggering placeholder.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageTriggerPlaceholder() {
    $build = MustacheRenderTemplate::build('some_test_template');
    $placeholder = ['#markup' => '<div class="click-me">Click me!</div>'];
    $build->withPlaceholder($placeholder);
    $build->usingDataFromUrl('/mustache-test/json');
    $build->withClientSynchronization()
      ->periodicallyRefreshesAt(1000)
      ->startsWhenElementWasTriggered('.click-me')
        ->atEvent('click')
        ->once();
    return [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
  }

  /**
   * Returns a plain page for testing a certain template with a limited triggering placeholder.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageLimitedTriggerPlaceholder() {
    $build = MustacheRenderTemplate::build('some_test_template');
    $placeholder = ['#markup' => '<div class="click-me">Click me!</div>'];
    $build->withPlaceholder($placeholder);
    $build->usingDataFromUrl('/mustache-test/json');
    $build->withClientSynchronization()
      ->periodicallyRefreshesAt(1000)
      ->upToNTimes(3)
      ->startsWhenElementWasTriggered('.click-me')
        ->atEvent('click')
        ->once();
    return [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
  }

  /**
   * Returns a page for testing a template with morphing DOM trees.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageTriggerMorphing() {
    $build = MustacheRenderTemplate::build('test_morph');
    $placeholder = ['#markup' => '<div class="click-me">Click me!</div>'];
    $build->withPlaceholder($placeholder);
    $build->usingDataFromUrl('/mustache-test/json');
    $build->withClientSynchronization()
      ->morphing()
      ->periodicallyRefreshesAt(1000)
      ->startsWhenElementWasTriggered('.click-me')
      ->atEvent('click')
      ->once();
    return [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
  }

  /**
   * Returns a page for testing an inline template with Javascript.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageEvalInline() {
    $build = MustacheRenderTemplate::build('test_eval_inline', '<script type="text/javascript">console.log("Executed as inline JS.");</script>');
    $build->withClientSynchronization()->executesInnerScripts(TRUE);
    return [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
  }

}
