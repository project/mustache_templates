<?php

namespace Drupal\mustache_test\Controller;

use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache_test\TestLinks;

/**
 * A controller delivering resources for testing Mustache templates with Tokens.
 */
class MustacheTestTokenController {

  /**
   * Returns a plain page for testing an inline template using Tokens.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function page() {
    $build = MustacheRenderTemplate::build('my_token_template', '
    {{#template.my_partial}}
    <div><b>Current site name:</b> {{#if.site.name.defined}}{{site.name}}{{/if.site.name.defined}}
    {{#previous.site.name}} ({{#if.site.name.equals.previous}}same as{{/if.site.name.equals.previous}}{{#if.site.name.greaterthan.previous}}greater than{{/if.site.name.greaterthan.previous}}{{#if.site.name.lessthan.previous}}less than{{/if.site.name.lessthan.previous}} the previous one)
    {{/previous.site.name}}
    </div>
      {{#if.previous.site.name.empty}}No previous data available, maybe because cache got cleared or nothing happened...{{/if.previous.site.name.empty}}
      {{#previous.site.name}}<div><b>Previous name:</b> {{previous.site.name}}</div>{{/previous.site.name}}
      {{#site.slogan}}<div><b>Slogan:</b> {{site.slogan}}</div>{{/site.slogan}}
    {{/template.my_partial}}
    <h1>Site information using Tokens:</h1>
    {{>my_partial}}
    ');
    $build->withTokens();

    $render = [
      $build->toRenderArray(),
      ['#markup' => TestLinks::get()],
    ];
    return $render;
  }

}
