<?php

namespace Drupal\mustache_test\Controller;

use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache\Render\Markup;
use Drupal\mustache_test\TestLinks;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * A controller delivering resources for testing form binding.
 */
class MustacheTestFormController {

  /**
   * Returns a Json response that echoes query parameters in uppercase.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   */
  public function echoFeed(Request $request) {
    $data = ['echo' => []];
    foreach ($request->query->keys() as $key) {
      $data['echo'][$key] = strtoupper($request->get($key, ''));
    }
    $response = new JsonResponse();
    $response->setPrivate();
    $response->setJson(json_encode($data));
    return $response;
  }

  /**
   * Returns a page with a client form that echoes submitted text input.
   *
   * @return array
   *   A build array for rendering the Mustache template.
   */
  public function pageClientEcho() {
    $render = [];

    $form = <<<HTML
<div>
    <form id="myform" method="GET" action="">
      <input id="myfield" type="text" name="s" value="" />
    </form>
</div>
HTML;

    $placeholder = ['#markup' => ''];
    $build = MustacheRenderTemplate::build('inline_result', '<b>Submitted</b>: {{form.s}}<br /><b>Received</b>: {{echo.s}}');
    $build->withPlaceholder($placeholder);
    $build->usingDataFromUrl('/mustache-test/echo-feed');
    $build->withClientSynchronization()
      ->usingFormValues('#myform')
      ->dataMayBeOfMaxAge(10000)
      ->startsWhenElementWasTriggered('#myfield')
      ->atEvent('input')
      ->always();

    $render[] = $build->toRenderArray();
    $render[] = ['#markup' => Markup::create($form)];
    $render[] = ['#markup' => TestLinks::get()];
    return $render;
  }

}
