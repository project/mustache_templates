<?php

namespace Drupal\mustache\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * The interface implemented by all Mustache magic variable plugins.
 */
interface MustacheMagicInterface extends PluginInspectionInterface, TrustedCallbackInterface, MarkupInterface {}
