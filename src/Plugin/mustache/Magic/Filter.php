<?php

namespace Drupal\mustache\Plugin\mustache\Magic;

use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mustache\Plugin\MustacheMagic;

/**
 * Various filter processing for template contents.
 *
 * Usage:
 *
 * You can use the {{#filter.<method>}} section variable to apply a text
 * processing filter using a specified <method> on template contents.
 *
 * @code
 * {{#filter.trim}}  My whitespace will be trimmed.   {{/filter.trim}}
 * {{#filter.striptags}}<p>My HTML tags will be removed.</p>{{/filter.striptags}}
 * {{#filter.nl2br}}The new line \n will be converted to a break tag.{{/filter.nl2br}}
 * {{#filter.spaceless}}Removes whitespace between HTML tags.{{/filter.spaceless}}
 * {{#filter.upper}}Makes the whole text uppercase.{{/filter.upper}}
 * {{#filter.lower}}Makes the whole text lowercase.{{/filter.lower}}
 * {{#filter.capitalize}}The first letter of the text will be uppercase.{{/filter.capitalize}}
 * {{#filter.truncate.100}}This text will be truncated when it exceeds 100 characters, and appended with a ... if it was truncated.{{/filter.truncate.100}}
 * {{#filter.path}}Hello There/GoodBye{{/filter.path}} will output "hello%20there/goodbye", suitable for using as a URL path or relative file path.
 * {{#filter.plus.10}}1{{/filter.plus.10}} will output "11".
 * {{#filter.minus.1}}1{{/filter.minus.1}} will output "0".
 * @endcode
 *
 * @MustacheMagic(
 *   id = "filter",
 *   label = @Translation("Filter"),
 *   description = @Translation("Use <b>{{#filter.&lt;method&gt;}}</b> to apply a text processing filter. Examples: <ul><li>{{#filter.trim}}  My whitespace will be trimmed.   {{/filter.trim}}</li><li>{{#filter.striptags}}&lt;p&gt;My HTML tags will be removed.&lt;/p&gt;{{/filter.striptags}}</li><li>{{#filter.nl2br}}The new line \\n will be converted to a break tag.{{/filter.nl2br}}</li><li>{{#filter.spaceless}}Removes whitespace between HTML tags.{{/filter.spaceless}}</li><li>{{#filter.upper}}MAKES THE WHOLE TEXT UPPERCASE.{{/filter.upper}}</li><li>{{#filter.lower}}makes the whole text lowercase.{{/filter.lower}}</li><li>{{#filter.capitalize}}The first letter of the text will be uppercase.{{/filter.capitalize}}</li><li>{{#filter.truncate.100}}This text will be truncated when it exceeds 100 characters, and appended with a ... if it was truncated.{{/filter.truncate.100}}</li><li>{{#filter.path}}Hello There/GoodBye{{/filter.path}} will output ""hello%20there/goodbye"", suitable for using as a URL path or relative file path.</li><li>{{#filter.plus.10}}1{{/filter.plus.10}} will output ""11"".</li><li>{{#filter.minus.1}}1{{/filter.minus.1}} will output ""0"".</li></ul>")
 * )
 */
class Filter extends MustacheMagic {

  use StringTranslationTrait;

  /**
   * Whether this object is a clone.
   *
   * @var bool
   */
  protected $cloned = FALSE;

  /**
   * The client library for magic translations.
   *
   * @var string
   */
  public static $library = 'mustache/magic.filter';

  /**
   * Available filter methods.
   *
   * @var array
   */
  public static $available = [
    'trim' => 'trim',
    'striptags' => 'strip_tags',
    'nl2br' => 'nl2br',
    'spaceless' => 'Drupal\mustache\Plugin\mustache\Magic\Filter::spaceless',
    'upper' => 'mb_strtoupper',
    'lower' => 'mb_strtolower',
    'capitalize' => 'ucfirst',
    'truncate' => 'Drupal\mustache\Plugin\mustache\Magic\Filter::truncate',
    'path' => 'Drupal\mustache\Plugin\mustache\Magic\Filter::path',
    'plus' => 'Drupal\mustache\Plugin\mustache\Magic\Filter::plus',
    'minus' => 'Drupal\mustache\Plugin\mustache\Magic\Filter::minus',
  ];

  /**
   * The contained method keys.
   *
   * @var string[]
   */
  protected array $keys = [];

  /**
   * Implementation of the magic __isset() method.
   */
  public function __isset($name): bool {
    return !empty($name) || (!is_null($name) && is_scalar($name) && (trim((string) $name) !== ''));
  }

  /**
   * Implementation of the magic __get() method.
   */
  public function __get($name) {
    if (!$this->cloned) {
      $cloned = clone $this;
      $cloned->cloned = TRUE;
      return $cloned->__get($name);
    }
    $this->keys[] = mb_strtolower(trim((string) $name));
    return $this;
  }

  /**
   * Implements the magic __invoke() method, used as higher-order section.
   */
  public function __invoke($template_content = NULL, $render = NULL) {
    if (!isset($template_content, $render)) {
      return;
    }

    // Attach the library at the beginning, so that BubbleableMetadata will
    // include that one (and will not forget about it).
    $this->attachLibrary();

    $rendered = $render($template_content);

    foreach ($this->keys as $i => $key) {
      if (!isset(static::$available[$key])) {
        continue;
      }
      $func = static::$available[$key];
      $args = isset($this->keys[$i + 1]) ? array_merge([$rendered], array_slice($this->keys, $i + 1)) : [$rendered];
      $rendered = $func(...$args);
    }

    return $rendered;
  }

  /**
   * Removes whitespace between HTML tags.
   *
   * @param mixed $text
   *   The text to process.
   *
   * @return string
   *   The processed text.
   */
  public static function spaceless($text): string {
    return trim(preg_replace('/>\s+</', '><', $text));
  }

  /**
   * Truncates the text.
   *
   * @param mixed $text
   *   The text to process.
   * @param mixed $size
   *   The target size.
   *
   * @return string
   *   The processed text.
   */
  public static function truncate($text, $size = NULL): string {
    if (!is_scalar($size) || !is_numeric($size)) {
      return $text;
    }
    $wordsafe = $size > 20;
    return Unicode::truncate((string) $text, (int) $size, $wordsafe, TRUE, $wordsafe ? $size - 10 : 1);
  }

  /**
   * Encodes the text suitable for URL path or path usage.
   *
   * @param mixed $text
   *   The text to process.
   *
   * @return string
   *   The processed text.
   */
  public static function path($text): string {
    return UrlHelper::encodePath(mb_strtolower($text));
  }

  /**
   * Performs an addition on the text.
   *
   * @param mixed $text
   *   The text to process.
   * @param mixed $number
   *   The number.
   *
   * @return string
   *   The processed text.
   */
  public static function plus($text, $number = NULL): string {
    if (!is_scalar($number) || !is_numeric($number)) {
      return $text;
    }
    $text = trim($text);
    if (!is_numeric($text)) {
      return $text;
    }
    $text = $text + $number;
    return (string) $text;
  }

  /**
   * Performs a subtraction on the text.
   *
   * @param mixed $text
   *   The text to process.
   * @param mixed $number
   *   The number.
   *
   * @return string
   *   The processed text.
   */
  public static function minus($text, $number = NULL): string {
    if (!is_scalar($number) || !is_numeric($number)) {
      return $text;
    }
    $text = trim($text);
    if (!is_numeric($text)) {
      return $text;
    }
    $text = $text - $number;
    return (string) $text;
  }

}
