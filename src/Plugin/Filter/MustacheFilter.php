<?php

namespace Drupal\mustache\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\mustache\Helpers\Mustache;
use Drupal\mustache\Helpers\MustacheRenderTemplate;
use Drupal\mustache\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter for processing Mustache template syntax.
 *
 * @Filter(
 *   id = "mustache",
 *   title = @Translation("Mustache template filter"),
 *   description = @Translation("Mustache template processor including token support for dynamic content. Example: {{site.name}}"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class MustacheFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (!Mustache::containsSyntax($text) && !Mustache::containsToken($text)) {
      return $result;
    }

    $template = MustacheRenderTemplate::build(hash('md4', $text), $text)
      ->withTokens();
    $build = $template->toRenderArray();
    $markup = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$build) {
      return $this->renderer->render($build);
    });
    $result = $result->merge(BubbleableMetadata::createFromRenderArray($build));
    $processed = empty($build['#use_sync']) && isset($build['#content']) ? Markup::create(trim((string) $build['#content'])) : Markup::create(trim((string) $markup));
    $result->setProcessedText($processed);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
      <p>You can write Mustache templates:</p>
      <ul>
        <li>Use tokens within Mustache brackets, for example {{site.name}}</li>
        <li>Also supports conditionals: {{#node.title}}&lt;h1&gt;{{node.title}}&lt;/h1&gt;{{/node.title}}</li>
        <li>See the <a href="https://mustache.github.io/mustache.5.html" target="_blank" rel="noopener noreferrer">Mustache manual</a> for more details.</li>
      </ul>');
    }
    else {
      return $this->t('You can write Mustache templates using tokens, e.g. {{node.title}}.');
    }
  }

}
