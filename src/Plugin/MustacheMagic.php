<?php

namespace Drupal\mustache\Plugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Mustache magic variable plugins.
 */
abstract class MustacheMagic extends PluginBase implements MustacheMagicInterface, ContainerFactoryPluginInterface {

  /**
   * The attached data to use for rendering the template.
   *
   * @var mixed
   */
  protected $data;

  /**
   * The template element as render array.
   *
   * @var array
   */
  protected $element;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger, if any.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface|null
   */
  protected $messenger;

  /**
   * The client library of this magic variable plugin used for dynamic includes.
   *
   * When a library should unconditionally be included, then the "library" key
   * of the plugin definition may be used instead.
   *
   * @var string
   */
  public static $library = NULL;

  /**
   * Constructs a MustacheMagicBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->data = &$configuration['data'];
    $this->element = &$configuration['element'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.mustache');
    $current_request = $container->has('request_stack') ? $container->get('request_stack')->getCurrentRequest() : NULL;
    if ($current_request && $container->has('current_user') && $container->get('current_user')->hasPermission('view mustache debug messages')) {
      $instance->messenger = $container->get('messenger');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return "";
  }

  /**
   * Implements the magic __serialize() method.
   *
   * @return array
   *   The array for serialization.
   */
  public function __serialize() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function jsonSerialize() {
    return NULL;
  }

  /**
   * Whether tokens are supposed to be included.
   *
   * @return bool
   *   Returns TRUE if tokens are enabled, FALSE otherwise.
   */
  protected function withTokens() {
    return isset($this->element['#with_tokens']) && $this->element['#with_tokens'] !== FALSE;
  }

  /**
   * Whether the wrapper template is inline or not.
   *
   * @return bool
   *   Returns TRUE if inline, FALSE otherwise.
   */
  protected function isInline() {
    return isset($this->element['#inline']) && ($this->element['#inline'] !== FALSE);
  }

  /**
   * Dynamically includes the defined client library as attachment.
   *
   * @param string|array|null $library
   *   An explicit library to attach. Omit this argument to use the statically
   *   defined library.
   */
  protected function attachLibrary($library = NULL) {
    if ((!isset(static::$library) && !isset($library)) || empty($this->element['#use_sync'])) {
      return;
    }

    if ($library) {
      $libraries = is_array($library) ? $library : [$library];
    }
    else {
      $libraries = is_array(static::$library) ? static::$library : [static::$library];
    }
    if (!empty($libraries)) {
      if (!isset($this->element['#attached']['library'])) {
        $this->element['#attached']['library'] = [];
      }
      $libs = &$this->element['#attached']['library'];
      foreach ($libraries as $lib) {
        if (!in_array($lib, $libs)) {
          $libs[] = $lib;
        }
      }
    }
  }

}
