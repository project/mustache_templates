<?php

namespace Drupal\mustache;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\State\StateInterface;

/**
 * Service class for the Mustache.php render engine.
 *
 * Mustache.php engine written by
 * Copyright (c) 2010-2015 Justin Hileman.
 */
class MustachePhpEngine extends \Mustache_Engine {

  /**
   * MustachePhpEngine constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The Drupal state.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache bin.
   * @param array $options
   *   See documentation of the Mustache_Engine class.
   */
  public function __construct(StateInterface $state, CacheBackendInterface $cache, array $options = []) {
    if (!isset($options['cache'])) {
      $cid = 'mustache:cache_prefix';
      if ($cached = $cache->get($cid)) {
        $prefix = $cached->data;
      }
      else {
        $prefix = $state->get('mustache_cache_prefix');
        if (!isset($prefix)) {
          $prefix = uniqid();
          $state->set('mustache_cache_prefix', $prefix);
        }
        $cache->set($cid, $prefix);
      }
      $options['cache'] = new MustachePhpCache($prefix);
    }
    elseif (empty($options['cache'])) {
      unset($options['cache']);
    }
    $loader = NULL;
    if (!isset($options['loader'])) {
      $loader = new MustachePhpLoader();
      $options['loader'] = $loader;
    }
    elseif (empty($options['loader'])) {
      unset($options['loader']);
    }
    if (!isset($options['partials_loader'])) {
      $options['partials_loader'] = isset($loader) ? $loader : new MustachePhpLoader();
    }
    elseif (empty($options['partials_loader'])) {
      unset($options['partials_loader']);
    }
    parent::__construct($options);
  }

  /**
   * Load a Mustache PHP template instance by inline content.
   *
   * @param string $content
   *   The inline template content.
   *
   * @return \Mustache_Template
   *   The loaded PHP template instance.
   */
  public function loadTemplate($content) {
    $loader = $this->getLoader();
    if ($loader instanceof \Mustache_Loader_MutableLoader) {
      $name = hash('md4', $content);
      $loader->setTemplate($name, $content);
      return parent::loadTemplate($name);
    }
    return parent::loadTemplate($content);
  }

  /**
   * Load a Mustache PHP template instance by its registered name.
   *
   * @param string $name
   *   The name of the template.
   *
   * @return \Mustache_Template
   *   The loaded PHP template instance.
   */
  public function loadTemplateByName($name) {
    return parent::loadTemplate($name);
  }

}
