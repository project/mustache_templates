<?php

namespace Drupal\mustache\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for Mustache magic variable plugins.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class MustacheMagic extends Plugin {

  /**
   * The plugin ID that is also being mapped as the root variable name.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the magic variable.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the magic variable.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The client library to always include when using DOM synchronization.
   *
   * @var string
   */
  public $library = '';

  /**
   * Whether the plugin is primarily used as a lambda (prefixed with "{{#").
   *
   * @var bool
   */
  public $lambda = TRUE;

  /**
   * Whether the top level acess of this variable always evaluates to be empty.
   *
   * Can be a string to specify a callable method of the plugin class that
   * returns a boolean indicating whether the top level is empty or not.
   *
   * @var bool|string
   */
  public $empty = TRUE;

}
