<?php

namespace Drupal\mustache;

/**
 * Mustache PHP template loader implementation.
 *
 * This loader behaves similar to the \Mustache_Loader_ArrayLoader
 * implementation, but includes the loading of registered templates.
 */
class MustachePhpLoader implements \Mustache_Loader, \Mustache_Loader_MutableLoader {

  /**
   * A currently known list of manually set template contents.
   *
   * @var string[]
   */
  protected $templates = [];

  /**
   * A currently known list of templates that got loaded from the registry.
   *
   * @var string[]
   */
  protected $loadedRegistry = [];

  /**
   * A currently known list of templates that are not loaded from registry.
   *
   * @var string[]
   */
  protected $loadedNotRegistry = [];

  /**
   * {@inheritdoc}
   */
  public function load($name) {
    if (!isset($this->templates[$name])) {
      try {
        $content = static::loadContent($name);
      }
      catch (\Exception $e) {
        $content = FALSE;
      }
      if ($content === FALSE) {
        throw new \Mustache_Exception_UnknownTemplateException($name);
      }
      // No caching of the content is made here, as the 'mustache.templates'
      // service itself already caches template contents in-memory.
      $this->loadedRegistry[$name] = $name;
      return $content;
    }

    $this->loadedNotRegistry[$name] = $name;
    return $this->templates[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplates(array $templates) {
    $this->templates = $templates;
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplate($name, $template) {
    $this->templates[$name] = $template;
  }

  /**
   * Get a list of template names that got loaded from the registry.
   *
   * @return string[]
   *   The list of loaded template names, both key and value are template names.
   */
  public function getLoadedRegistryTemplateNames() {
    return $this->loadedRegistry;
  }

  /**
   * Resets the list of known templates that got loaded from the registry.
   */
  public function resetLoadedRegistryTemplateList() {
    $this->loadedRegistry = [];
  }

  /**
   * Get a list of loaded template names that not got loaded from the registry.
   *
   * @return string[]
   *   The list of loaded template names, both key and value are template names.
   */
  public function getLoadedNotRegistryTemplateNames() {
    return $this->loadedNotRegistry;
  }

  /**
   * Resets the list of loaded templates that got not loaded from the registry.
   */
  public function resetLoadedNotRegistryTemplateList() {
    $this->loadedNotRegistry = [];
  }

  /**
   * Load the template content for the given template name.
   *
   * @param string $name
   *   The template name to load the content for.
   *
   * @return string|false
   *   The content of the template, or FALSE if the content could not be read.
   */
  protected static function loadContent($name) {
    /** @var \Drupal\mustache\MustacheTemplates $templates */
    $templates = \Drupal::service('mustache.templates');
    return $templates->getContent($name);
  }

}
