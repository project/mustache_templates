<?php

namespace Drupal\mustache\Magic;

use Drupal\Component\Utility\NestedArray;
use Drupal\mustache\Plugin\MustacheMagicManager;

/**
 * Factory for creating loaders of Mustache magic variable plugins.
 */
class MagicFactory {

  /**
   * The manager of Mustache magic variable plugins.
   *
   * @var \Drupal\mustache\Plugin\MustacheMagicManager
   */
  protected $pluginManager;

  /**
   * The MagicFactory constructor.
   *
   * @param \Drupal\mustache\Plugin\MustacheMagicManager $plugin_manager
   *   The manager of Mustache magic variable plugins.
   */
  public function __construct(MustacheMagicManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * Creates lazy loaders for all available magic variable plugins.
   *
   * @param mixed &$data
   *   The attached data to use for rendering the template.
   * @param array &$element
   *   The element of the template as render array.
   * @param array $further_config
   *   (Optional) Further configuration to be passed to the variable plugins.
   *
   * @return array
   *   A nested array of lazy loaders, keyed by their base plugin ID.
   */
  public function createInstances(&$data, array &$element, array $further_config = []) {
    $instances = [];
    $configuration = [
      'data' => &$data,
      'element' => &$element,
    ] + $further_config;
    foreach ($this->pluginManager->getDefinitions() as $plugin_id => $definition) {
      NestedArray::setValue($instances, explode(':', $plugin_id), new LazyMagic($this->pluginManager, $plugin_id, $configuration, $definition['empty'] ?? TRUE));
      if (!empty($definition['library']) && !empty($element['#use_sync']) && (empty($element['#attached']['library']) || !in_array($definition['library'], $element['#attached']['library']))) {
        $element['#attached']['library'][] = $definition['library'];
      }
    }
    return $instances;
  }

}
