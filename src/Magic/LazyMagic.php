<?php

namespace Drupal\mustache\Magic;

use Drupal\Component\Render\MarkupInterface;
use Drupal\mustache\Plugin\MustacheMagicManager;

/**
 * Lazy-loading of a Mustache magic variable plugin for a specific template.
 */
class LazyMagic implements MarkupInterface {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The manager of Mustache magic variable plugins.
   *
   * @var \Drupal\mustache\Plugin\MustacheMagicManager
   */
  protected $pluginManager;

  /**
   * The instantiated plugin object, or NULL if not yet loaded.
   *
   * @var \Drupal\mustache\Plugin\MustacheMagicInterface|null
   */
  protected $plugin;

  /**
   * Whether the top level acess of this variable always evaluates to be empty.
   *
   * @var bool|string
   */
  protected $empty;

  /**
   * The LazyMagic constructor.
   *
   * @param \Drupal\mustache\Plugin\MustacheMagicManager $plugin_manager
   *   The manager of Mustache magic variable plugins.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $configuration
   *   The plugin configuration.
   * @param bool|string $empty
   *   Whether the top level acess of this variable always evaluates to be
   *   empty. Can be a string to specify a callable method of the plugin class
   *   that returns a boolean indicating whether the top level is empty or not.
   */
  public function __construct(MustacheMagicManager $plugin_manager, $plugin_id, array $configuration, $empty = TRUE) {
    $this->pluginManager = $plugin_manager;
    $this->pluginId = $plugin_id;
    $this->configuration = $configuration;
    $this->empty = $empty;
  }

  /**
   * Implementation of the magic __invoke() method.
   */
  public function __invoke(...$args) {
    if (!empty($this->configuration['element']['#placeholder'])) {
      throw new \LogicException("Placeholder is not empty, but a magic plugin got invoked. The magic variable must only be invoked by the Mustache.php renderer, and nothing else.");
    }
    $plugin = $this->plugin();
    if (method_exists($plugin, '__invoke')) {
      return $plugin(...$args);
    }
  }

  /**
   * Implementation of the magic __toString() method.
   */
  public function __toString() {
    return (string) $this->plugin();
  }

  /**
   * Implementation of the magic __call() method.
   */
  public function __call($name, $arguments) {
    $plugin = $this->plugin();
    if (method_exists($plugin, $name) && in_array($name, $plugin::trustedCallbacks())) {
      return $plugin->$name($arguments);
    }
  }

  /**
   * Implementation of the magic __isset() method.
   */
  public function __isset(string $name): bool {
    $plugin = $this->plugin();
    // We handle methods to be automatically invoked and give them a higher
    // precedence than properties.
    // @see https://github.com/bobthecow/mustache.php/wiki/Magic-Methods
    return method_exists($plugin, $name) && in_array($name, $plugin::trustedCallbacks()) ? TRUE : isset($plugin->$name);
  }

  /**
   * Implementation of the magic __get() method.
   */
  public function __get($name) {
    $plugin = $this->plugin();
    return method_exists($plugin, $name) && in_array($name, $plugin::trustedCallbacks()) ? $plugin->$name() : $plugin->$name;
  }

  /**
   * Implements the magic __serialize() method.
   *
   * @return array
   *   The array for serialization.
   */
  public function __serialize() {
    $plugin = $this->plugin();
    if (method_exists($plugin, '__serialize')) {
      return $plugin->__serialize();
    }
    return [];
  }

  /**
   * Whether the top level acess of this variable evaluates to be empty.
   */
  public function isEmpty() {
    if (is_bool($this->empty)) {
      return $this->empty;
    }
    $plugin = $this->plugin();
    if (method_exists($plugin, $this->empty)) {
      return call_user_func([$plugin, $this->empty]);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  #[\ReturnTypeWillChange]
  public function jsonSerialize() {
    return $this->plugin()->jsonSerialize();
  }

  /**
   * Loads the plugin instance.
   *
   * @return \Drupal\mustache\Plugin\MustacheMagicInterface
   *   The plugin instance.
   */
  public function plugin() {
    if (isset($this->plugin)) {
      return $this->plugin;
    }
    $this->plugin = $this->pluginManager->createInstance($this->pluginId, $this->configuration);
    return $this->plugin();
  }

}
