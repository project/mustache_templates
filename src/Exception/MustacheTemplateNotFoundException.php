<?php

namespace Drupal\mustache\Exception;

/**
 * Class MustacheTemplateNotFoundException.
 */
class MustacheTemplateNotFoundException extends MustacheException {}
