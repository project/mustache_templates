<?php

namespace Drupal\mustache\Exception;

/**
 * Class MustacheFileException.
 */
class MustacheFileException extends MustacheException {}
