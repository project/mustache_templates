<?php

namespace Drupal\mustache\Exception;

/**
 * Class MustacheTemplateRenderPreventedException.
 */
class MustacheTemplateRenderPreventedException extends MustacheException {}
