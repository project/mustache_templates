<?php

namespace Drupal\mustache\Exception;

/**
 * Class MustacheException.
 */
class MustacheException extends \RuntimeException {}
