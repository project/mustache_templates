<?php

namespace Drupal\mustache\Helpers;

/**
 * Little helpers when working with Mustache templates.
 */
class Mustache {

  /**
   * Checks whether the given string contains Mustache template syntax.
   *
   * @param string $string
   *   The string to check for.
   *
   * @return bool
   *   Returns TRUE if it contains Mustache syntax, FALSE otherwise.
   */
  public static function containsSyntax($string) {
    if (is_object($string) && method_exists($string, '__toString')) {
      $string = (string) $string;
    }
    return is_string($string)
      && strpos($string, '{{') !== FALSE
      && strpos($string, '}}')
      && 1 === preg_match('/\{\{[^a-zA-Z\d\_\-\{\#\&\^]*([a-zA-Z\d\_\-\?\<\>\/\.]+)[^a-zA-Z\d\_\-\.\}]*\}\}/x', $string);
  }

  /**
   * Checks whether the given string contains at least one Token.
   *
   * @param string $string
   *   The string to check for.
   *
   * @return bool
   *   Returns TRUE if it contains at least one Token, FALSE otherwise.
   */
  public static function containsToken($string) {
    if (is_object($string) && method_exists($string, '__toString')) {
      $string = (string) $string;
    }
    return is_string($string)
      && strpos($string, '[') !== FALSE
      && strpos($string, ':')
      && strpos($string, ']')
      && \Drupal::token()->scan($string);
  }

}
