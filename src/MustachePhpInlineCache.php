<?php

namespace Drupal\mustache;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * The Mustache PHP inline template cache.
 *
 * This is a "middleware" caching layer between the Mustache.php engine and its
 * originally used cache instance, to handle inline templates differently.
 * Inline templates should not be cached on the file system, because they
 * might be dynamically generated, so that the overall amount of inline
 * template variants cannot be estimated at all. This inline layer does not
 * store inline templates on the file system, reducing the risk of disk
 * usage flooding.
 */
class MustachePhpInlineCache extends \Mustache_Cache_AbstractCache {

  /**
   * The cache bin to store compiled inline templates.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The "original" cache instance used my the Mustache.php engine.
   *
   * @var \Mustache_Cache
   */
  protected $engineCache;

  /**
   * The MustachePhpInlineCache constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache bin to store compiled inline templates.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function load($key) {
    if (class_exists($key, FALSE) || (isset($this->engineCache) && $this->engineCache !== $this && $this->engineCache->load($key))) {
      return TRUE;
    }
    $cid = 'mustache:inline_php:' . $key;
    if ($cached = $this->cache->get($cid)) {
      eval('?>' . $cached->data);
    }
    return class_exists($key, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function cache($key, $value) {
    $cid = 'mustache:inline_php:' . $key;
    $this->cache->set($cid, $value);
    if (!class_exists($key, FALSE)) {
      eval('?>' . $value);
    }
  }

  /**
   * Set the "original" cache used by the Mustache.php engine.
   *
   * @param \Mustache_Cache $engine_cache
   *   The cache instance.
   */
  public function setEngineCache(\Mustache_Cache $engine_cache) {
    $this->engineCache = $engine_cache;
  }

}
