<?php

namespace Drupal\mustache\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Dispatches before rendering the contents of a Mustache template.
 */
class BeforeTemplateRenderEvent extends Event {

  /**
   * The name of the template.
   *
   * @var mixed
   */
  protected $name;

  /**
   * The content of the template.
   *
   * @var mixed
   */
  protected $content;

  /**
   * The reference to the template instance.
   *
   * @var mixed
   */
  public $template;

  /**
   * The reference to the data that will be passed to the rendering process.
   *
   * @var mixed
   */
  public $data;

  /**
   * The BeforeTemplateRenderEvent constructor.
   *
   * @param mixed $name
   *   The name of the template.
   * @param mixed $content
   *   The content of the template.
   * @param mixed &$template
   *   The reference to the template instance.
   * @param mixed &$data
   *   The reference to the data that will be passed to the rendering process.
   */
  public function __construct($name, $content, &$template, &$data) {
    $this->name = $name;
    $this->content = $content;
    $this->template = &$template;
    $this->data = &$data;
  }

  /**
   * Get the template name.
   *
   * @return mixed
   *   The template name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get the template content.
   *
   * @return mixed
   *   The template content.
   */
  public function getContent() {
    return $this->content;
  }

}
