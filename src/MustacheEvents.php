<?php

namespace Drupal\mustache;

/**
 * Defines events provided by the Mustache templates module.
 */
final class MustacheEvents {

  /**
   * Dispatched before rendering the contents of a Mustache template.
   *
   * This event allows modules to perform an action whenever a template is
   * about to be rendered. The event listener method receives a
   * \Drupal\mustache\Event\BeforeTemplateRenderEvent instance.
   *
   * @Event
   *
   * @var string
   */
  const BEFORE_TEMPLATE_RENDER = 'mustache.template_render.before';

}
