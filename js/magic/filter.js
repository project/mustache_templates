/**
 * @file
 * Magic filters for Mustache templates.
 */

(function (sync) {
  var magicFilter = function (buffer, props) {
    return new Proxy(buffer, Object.assign({
      keys: [],
      available: {trim: 0, striptags: 0, nl2br: 0, spaceless: 0, upper: 0, lower: 0, capitalize: 0, truncate: 1, path: 0, plus: 1, minus: 1},
      cloned: false,
      get: function (buffer, property, receiver) {
        if (!this.cloned) {
          return magicFilter(buffer, {cloned: true})[property];
        }
        this.keys.push(property);
        if (this.available[property]) {
          return receiver;
        }
        return function (wrapper) {
          return function(wrapper, text, render) {
            var k, m, param, rendered = render(text);
            for (var i = 0; i < wrapper.keys.length; i++) {
              k = wrapper.keys[i];
              m = wrapper[k];
              if ((typeof m === 'function') && (typeof wrapper.available[k] === 'number')) {
                if (wrapper.available[k]) {
                  param = wrapper.keys[i + 1];
                }
                rendered = m(rendered, param);
              }
            }
            return rendered;
          }.bind(wrapper, wrapper);
        }.bind(this, this);
      },
      has: function(target, prop) {
        return true;
      },
      trim: function (text) {
        return text.trim();
      },
      striptags: function (text) {
        var el = document.createElement('div');
        el.innerHTML = text;
        return el.textContent || el.innerText;
      },
      nl2br: function (text) {
        return text.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br/>$2');
      },
      spaceless: function (text) {
        return text.replace(/>\s+</g,'><').trim();
      },
      upper: function (text) {
        return text.toUpperCase();
      },
      lower: function (text) {
        return text.toLowerCase();
      },
      capitalize: function (text) {
        return text.charAt(0).toUpperCase() + text.slice(1);
      },
      truncate: function (text, size) {
        if (typeof size === 'string') {
          size = parseInt(size);
        }
        if ((typeof size === 'number') && (text.length > size)) {
          text = (text += " ").substring(0, size);
          text = text.substring(0, Math.min(text.length, text.lastIndexOf(" ")));
          text += '...';
        }
        return text;
      },
      path: function (text) {
        return encodeURIComponent(text.toLowerCase()).replace('%2F', '/');
      },
      plus: function (text, number) {
        text = text.trim();
        if (isNaN(text) || isNaN(number)) {
          return text;
        }
        return +text + +number;
      },
      minus: function (text, number) {
        text = text.trim();
        if (isNaN(text) || isNaN(number)) {
          return text;
        }
        return +text - +number;
      }
    }, props));
  };
  sync.registry.magic.filter = function (buffer) {
    return magicFilter(buffer, {});
  };
}(mustacheSync));
