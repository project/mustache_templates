/**
 * @file
 * Magic translations for Mustache templates.
 */

(function (sync, settings) {
  var translate = function (wrapper, langcode, text, render) {
    var i, k, trans_template, from_langcode, to_langcode, item, reg = sync.registry;
    for (i in reg.templates) {
      if (reg.templates[i] === text && i.indexOf('trans-') === 0) {
        i = i.slice(6);
        from_langcode = i.slice(0, i.indexOf('-'));
        i = i.slice(from_langcode.length + 1);
        to_langcode = i.slice(0, i.indexOf('-'));
        trans_template = i.slice(to_langcode.length + 1);
        if (langcode && langcode !== to_langcode) {
          continue;
        }
        for (k in reg.items) {
          item = reg.items[k];
          if (trans_template === item.template) {
            if ((item.provider && (!item.provider.fetched || item.provider.faulty)) || !item.ready() || !item.data) {
              throw {retry: [item, wrapper], component: "Translation"};
            }
            else if (item.provider && item.provider.fetching) {
              throw {retry: [wrapper], component: "Translation"};
            }
            return sync.___internals.item.render.call(item);
          }
        }
        break;
      }
    }
    return render(text);
  };
  sync.registry.magic.t = function (buffer) {
    var t;
    var l;
    if (settings.mustache && settings.mustache.lang) {
      t = {};
      for (l in settings.mustache.lang) {
        t[l] = function (wrapper, langcode) {return translate.bind(this, wrapper, langcode);}.bind(this, this, settings.mustache.lang[l]);
      }
    }
    else {
      t = function (wrapper, langcode) {return translate.bind(this, wrapper, langcode);}.bind(this, this, settings.path.currentLanguage);
    }
    return t;
  };
}(mustacheSync, drupalSettings));
