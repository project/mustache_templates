/**
 * @file
 * Object binding in Mustache templates.
 */

(function (sync) {
  var internals = sync.___internals.item;
  var init = internals.init;
  var ready = internals.ready;
  var update = internals.update;
  var prepare = internals.prepare;
  var subset = sync.___internals.subset;
  var getValue = function (select) {
    var value = false;
    if (this.data) {
      value = subset(this.data, select);
    }
    if (value === false && window) {
      value = subset(window, select);
    }
    return value;
  };
  internals.init = function () {
    var selects;
    var i;
    this.object = this.object || null;
    if (this.object) {
      if (typeof this.object === 'string') {
        this.object = this.object.split('+');
      }
      if (Array.isArray(this.object)) {
        this.object = {selects: this.object, values: {}};
      }
      else if (typeof this.object.selects === 'string') {
        this.object.selects = [this.object.selects];
      }
      if (Array.isArray(this.object.selects)) {
        selects = this.object.selects;
        for (i = 0; i < selects.length; i++) {
          if (typeof selects[i] === 'string') {
            selects[i] = selects[i].split('.');
          }
        }
      }
      else {
        this.object = false;
      }
    }
    return init.call(this);
  };
  internals.ready = function () {
    var selects;
    var i;
    if (this.object === false) {
      return false;
    }
    if (this.object) {
      selects = this.object.selects;
      for (i = 0; i < selects.length; i++) {
        if (getValue.call(this, selects[i]) === false) {
          return false;
        }
      }
    }
    return ready.call(this);
  };
  internals.update = function (period, force_fetch, no_repeat) {
    var size = 0;
    var selects;
    var select;
    var values;
    var value;
    var key;
    var i;

    if (this.object) {
      values = {};
      selects = this.object.selects;
      for (i = 0; i < selects.length; i++) {
        select = selects[i];
        key = select[select.length - 1];
        value = getValue.call(this, select);
        if (value === false || typeof value === 'function') {
          continue;
        }
        if (!this.object.values) {
          this.object.values = {};
        }
        this.object.values[key] = value;
        if (!(typeof value === 'string')) {
          try {
            value = JSON.stringify(value);
          }
          catch (e) {
            continue;
          }
        }
        value = encodeURIComponent(value);
        values[key] = value;
        size++;
      }
    }

    if (size && this.provider) {
      this.provider = this.provider.rebuild(values);
    }

    return update.call(this, period, force_fetch, no_repeat);
  };
  internals.prepare = function (rendered, buffer) {
    if (typeof buffer.data === 'object') {
      if (this.object && this.object.values) {
        buffer.data.object = Object.assign({}, this.object.values);
      }
    }
    return prepare.call(this, rendered, buffer);
  };
}(mustacheSync));
