/**
 * @file
 * Form binding in Mustache templates.
 */

(function (sync) {
  var internals = sync.___internals.item;
  var init = internals.init;
  var ready = internals.ready;
  var update = internals.update;
  var prepare = internals.prepare;
  internals.init = function () {
    this.form = this.form || null;
    if (typeof this.form === 'string') {
      this.form = {selector: this.form, el: null, values: {}};
    }
    return init.call(this);
  };
  internals.ready = function () {
    if (this.form && this.form.selector && !this.form.el) {
      this.form.el = document.querySelector(this.form.selector);
      if (!this.form.el) {
        return false;
      }
    }
    return ready.call(this);
  };
  internals.update = function (period, force_fetch, no_repeat) {
    var size = 0;
    var elements;
    var element;
    var values;
    var value;
    var i;

    if (this.form && this.form.selector) {
      this.form.el = document.querySelector(this.form.selector);
      if (this.form.el && this.form.el.elements) {
        size = this.form.el.length;
        elements = this.form.el.elements;
        values = {};
        for (i = 0; i < size; i++) {
          element = elements[i];
          if (typeof element.name !== 'undefined' && typeof element.value !== 'undefined' && !element.disabled && element.name !== 'form_build_id') {
            if (typeof element.type !== 'undefined' && (element.type === 'checkbox' || element.type === 'radio') && !element.checked) {
              this.form.values[element.name] = false;
            }
            else {
              this.form.values[element.name] = element.value;
            }
            value = this.form.values[element.name];
            if (!(typeof value === 'string')) {
              value = JSON.stringify(value);
            }
            value = encodeURIComponent(value);
            values[element.name] = value;
          }
        }
      }

      if (size && this.provider) {
        this.provider = this.provider.rebuild(values);
      }
    }

    return update.call(this, period, force_fetch, no_repeat);
  };
  internals.prepare = function (rendered, buffer) {
    if (typeof buffer.data === 'object') {
      if (this.form && this.form.values) {
        buffer.data.form = Object.assign({}, this.form.values);
      }
    }
    return prepare.call(this, rendered, buffer);
  };
}(mustacheSync));
