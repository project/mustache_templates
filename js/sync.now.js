/**
 * @file
 * Executes DOM content synchronization with Mustache templates.
 */

(function (Drupal, sync) {
  sync.now();

  Drupal.behaviors.mustacheSync = {
    attach: function attach(dom) {
      sync.refresh(dom);
    }
  };
}(Drupal, mustacheSync));
